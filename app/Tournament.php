<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Tournament extends Model
{
    protected $table = 'tournaments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'name', 'short_name', 'logo', 'category_id', 'user_id',  'deadline', 'deadline_subscription',
    ];

    /**
     * Get the user record associated with the result.
     */
    public function user()
    {
        return $this->belongsToMany('App\User', 'tournaments_users');
    }

    /**
     * Get the category record associated with the category.
     */
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    /**
     * Check if deadline is passed
     */
    public function scopeIsAvailable() {
        return $this->where('deadline_subscription', '>=', Carbon::now());
    }

    /**
     * Get the tournament available for current user
     *
     * @return \Illuminate\Support\Collection
     */
    public static function getAvailable($subscribed = true) {
        $tournaments = DB::table('tournaments')
            ->select('tournaments.*', 'tournaments_users.user_id as tournament_user_id')
            ->leftJoin('tournaments_users', function($join)
            {
                $join->on('tournaments_users.tournament_id', '=', 'tournaments.id');
                $join->on('tournaments_users.user_id', '=', DB::raw(Auth::user()->id));
            })
            ->where('deadline_subscription', '>=', Carbon::now());

        if (!$subscribed) {
            $tournaments = $tournaments->whereNull('tournaments_users.user_id');
        }

        return $tournaments->get();

    }

    /**
     * Get the tournament available a for current user
     *
     * @return \Illuminate\Support\Collection
     */
    public static function countAvailableNotSubscribed() {
        return DB::table('tournaments')
            ->select('tournaments.*', 'tournaments_users.user_id as tournament_user_id')
            ->leftJoin('tournaments_users', function($join)
            {
                $join->on('tournaments_users.tournament_id', '=', 'tournaments.id');
                $join->on('tournaments_users.user_id', '=', DB::raw(Auth::user()->id));
            })
            ->where('deadline_subscription', '>=', Carbon::now())
            ->whereNull('tournaments_users.user_id')
            ->count();

    }

    /**
     * Get the phase record associated with the Tournament.
     */
    public function phase()
    {
        return $this->hasMany('App\Phase');
    }

    /**
     * Return the path with the users avatars
     *
     * @return string
     */
    public function getLogo()
    {
        if ($this->logo) {
            return config('custom.tournaments_logo_path') . $this->logo;
        }
        return config('custom.tournaments_logo_path')  . config('custom.default_tournament_logo');
    }

    /**
     * Get the users record associated with the tournament.
     */
    public function users()
    {
        return $this->belongsToMany('App\User', 'tournaments_users');
    }


}
