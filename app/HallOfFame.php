<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HallOfFame extends Model
{
    use HasFactory;

    protected $fillable = [
        'tournament_id',
        'first_user_ids',
        'second_user_ids',
        'third_user_ids',
        'first_points_user_ids',
        'total_points_first',
        'second_points_user_ids',
        'total_points_second',
        'third_points_user_ids',
        'total_points_third',
        'best_odd_wins',
        'total_best_odd_win',
        'high_prediction_win_user_ids',
        'total_high_prediction_win',
    ];

    public $timestamps = true;

    protected $casts = [
        'first_user_ids' => 'array',
        'second_user_ids' => 'array',
        'third_user_ids' => 'array',
        'first_points_user_ids' => 'array',
        'second_points_user_ids' => 'array',
        'third_points_user_ids' => 'array',
        'best_odd_wins' => 'array',
        'high_prediction_win_user_ids' => 'array',
    ];

    public function tournament()
    {
        return $this->belongsTo(Tournament::class);
    }

    public function firstUsers()
    {
        return $this->first_user_ids ? User::whereIn('id', $this->first_user_ids)->get() : collect();
    }

    public function secondUsers()
    {
        return $this->second_user_ids ? User::whereIn('id', $this->second_user_ids)->get() : collect();
    }

    public function thirdUsers()
    {
        return $this->third_user_ids ? User::whereIn('id', $this->third_user_ids)->get() : collect();
    }

    public function firstPointsUsers()
    {
        return $this->first_points_user_ids ? User::whereIn('id', $this->first_points_user_ids)->get() : collect();
    }

    public function secondPointsUsers()
    {
        return $this->second_points_user_ids ? User::whereIn('id', $this->second_points_user_ids)->get() : collect();
    }

    public function thirdPointsUsers()
    {
        return $this->third_points_user_ids ? User::whereIn('id', $this->third_points_user_ids)->get() : collect();
    }

    public function bestOddWinUsers()
    {
        return $this->best_odd_win_user_ids ? User::whereIn('id', $this->best_odd_win_user_ids)->get() : collect();
    }

    public function highPredictionWinUsers()
    {
        return $this->high_prediction_win_user_ids ? User::whereIn('id', $this->high_prediction_win_user_ids)->get() : collect();
    }
}
