<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    protected $table = 'results';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'bet_id', 'event_id', 'user_id', 'win'
    ];

    /**
     * Get the bet record associated with the result.
     */
    public function bet()
    {
        return $this->belongsTo('App\Bet');
    }

    /**
     * Get the user record associated with the result.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the event record associated with the result.
     */
    public function event()
    {
        return $this->hasOne('App\Event');
    }
}
