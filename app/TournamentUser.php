<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TournamentUser extends Model
{
    protected $table = 'tournaments_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'tournament_id', 'user_id',
    ];

    /**
     * Return true if there is an occurrence with $userId and $tournamentId
     *
     * @param int $userId
     * @param int $tournamentId
     * @return boolean
     */
    public function isAuthorized($userId, $tournamentId) {
        return !$this
            ->where(['tournament_id' => $tournamentId, 'user_id' => $userId])
            ->get()
            ->isEmpty();
    }
}
