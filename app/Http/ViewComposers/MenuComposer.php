<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use JeroenNoten\LaravelAdminLte\AdminLte;
use App\User;
use Auth;

class MenuComposer
{
    private $adminlte;

    public function __construct(
        AdminLte $adminlte
    ) {
        $this->adminlte = $adminlte;
    }
    
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $user = User::where('id', '=', Auth::user()->id)
            ->with('Tournament')
            ->get();

        $view->with('user', $user);
        $view->with('adminlte', $this->adminlte);
    }

}