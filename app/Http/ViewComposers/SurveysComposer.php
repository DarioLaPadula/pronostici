<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Survey;
use App\SurveyResult;
use Auth;

class SurveysComposer
{
    //public $movieList = [];

    /**
     * Create a movie composer.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view)
    {
        return null;
        $surveys = null;
        $ids = [3, 4];

        if (Auth::user()->id != 1) {
            $surveyResult = SurveyResult::select('surveys.id')
                ->join('surveys_choices', 'surveys_choices.id', 'surveys_results.choice_id')
                ->join('surveys', 'surveys_choices.survey_id', 'surveys.id')
                ->whereIn('surveys_choices.survey_id', $ids)
                ->where('surveys_results.user_id', '=', Auth::user()->id)
                ->get();

            $ids = array_diff($ids, Collect($surveyResult)->pluck('id')->toArray());

            if ($ids) {

                $surveys = Survey::with('survey_choice.survey_results')
                    ->whereIn('id', $ids)
                    ->get();
            }

        }

        $view->with('surveys', $surveys);
    }
}