<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;
use App\Event;
use App\Bet;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Carbon;
use Auth;

class EventController extends Controller
{
    private $competitions;

    public function __construct()
    {
        $this->competitions = [
            'Uefa Champions League', 'Uefa Europa League', 'Uefa Euro U21', 'Uefa Euro 2021', 'Coppa America',
            'Coppa d\'Africa', 'Uefa Europa Conference League', 'Mondiali'];
    }

    /**
     * Route to create
     */
    public function available_online_multisport()
    {
        $url = 'http://xml.cdn.betclic.com/odds_it.xml';

        $context = stream_context_create(array('http' => array('header' => 'Accept: application/xml')));

        $competitions = ['Europa League', 'Euro U21', 'Euro 2021', 'Coppa America'];

        $xml = file_get_contents($url, false, $context);
        $xml = simplexml_load_string($xml, "SimpleXMLElement", LIBXML_NOBLANKS | LIBXML_NOBLANKS);
        /*  $json = json_encode($xml);
          $array = json_decode($json,TRUE);*/
        $events = [];


        foreach ($xml->sport as $sport) {
            $matchKey = 0;
            //if ((string)$sport->attributes()->{'name'} == 'Calcio') {
            foreach ($sport->event as $event) {
                $name = $event->attributes()->{'name'};

                if (str_contains((string)strtolower($name), 'olimpiadi') || str_contains((string)strtolower($name), 'parigi') || $sport->attributes()->{'name'} == 'Speciali Olimpiadi'
                    || (str_contains((string)strtolower($name), 'olympic')) || (str_contains((string)strtolower($name), 'paris'))

                ) {

                    $matches = $event->children();

                    foreach ($matches as $match) {

                        if (!isset($match['@attributes']['name']) && !isset($match['name'])) {
                            continue;
                        } else {
                            $name = isset($match['@attributes']['name']) ? $match['@attributes']['name'] : $match['name'];
                            $startDate = isset($match['@attributes']['start_date']) ? $match['@attributes']['start_date'] : $match['start_date'];
                        }

                        $events[] = [
                            'key' => $matchKey++,
                            'category_id' => 9,
                            'name' => $name,
                            'sport' => $sport->attributes()->{'id'},
                            'deadline' => Carbon::parse($startDate, 'UTC')->tz('Europe/Rome')->format('Y-m-d H:i'),
                            'competition' => $sport->attributes()->{'name'} . ' - ' . (string)$event->attributes()->{'name'}
                        ];
                    }

                    if (($competitionKey = array_search((string)$event->attributes()->{'name'}, $competitions)) !== false) {
                        unset($competitions[$competitionKey]);
                    }

                    if (empty($competitions)) {
                        break;
                    }

                    continue;
                }
            }
            //   }
        }

        /*foreach ($matches as $key => $match) {
            $events[] = [
                'key' => $key,
                'category_id' => 2,
                'name' => $match['@attributes']['name'],
                'deadline' => Carbon::parse($match['@attributes']['start_date'])->addHour()->format('Y-m-d H:i'),
            ];
        }*/

        return view('admin.events.available_online', [
            'events' => $events,
            'multisport' => 1,
        ]);
    }

    public function available_online()
    {

        //https://www.reloadbet85.com/api/coupons/eventpathtree?periodType=PRE_MATCH

        // https://www.reloadbet85.com/api/coupons/coupons?eventId=5470571

        //https://www.reloadbet85.com/api/coupons/coupons?sortBy=date&leagueIds=23698

        $context = stream_context_create(array('http' => array('header' => 'Accept: application/json')));
        $url = 'https://www.reloadbet85.com/api/coupons/eventpathtree?periodType=PRE_MATCH';

        $response = file_get_contents($url, false, $context);

        if ($response === false) {
            die('Errore durante il recupero dei dati dall\'API eventpathtree');
        }

        // Decodifica del JSON
        $data = json_decode($response, true);

        if ($data === null) {
            die('Errore durante la decodifica del JSON eventpathtree');
        }

        $found_competitions = [];
        $events = [];
        $matchKey = 0;

        foreach ($data as $sport) {
            if (isset($sport['regions'])) {
                foreach ($sport['regions'] as $region) {
                    if (isset($region['leagues'])) {
                        foreach ($region['leagues'] as $league) {
                            if (isset($league['id']) &&
                                isset($league['name']) &&
                                in_array(strtolower($league['name']), array_map('strtolower', $this->competitions))
                            ) {
                                $found_competitions[] = $league['id'];
                            }
                        }
                    }
                }
            }
        }

        foreach ($found_competitions as $competition) {
            $context = stream_context_create(array('http' => array('header' => 'Accept: application/json')));
            $url = 'https://www.reloadbet85.com/api/coupons/coupons?sortBy=date&leagueIds=' . $competition;

            $response = file_get_contents($url, false, $context);

            if ($response === false) {
                die('Errore durante il recupero dei dati dall\'API api/coupons');
            }

            // Decodifica del JSON
            $data = json_decode($response, true);

            if ($data === null || !isset($data['events'])) {
                die('Errore durante la decodifica del JSON api/coupons');
            }

            foreach ($data['events'] as $event) {
                $events[] = [
                    'key' => $event['id'],
                    //  'category_id' => 9,
                    'name' => $event['description'],
                    'deadline' => Carbon::parse($event['datetime'], 'UTC')->tz('Europe/Rome')->format('Y-m-d H:i'),
                    'competition' => $event['leagueDescription']
                ];
            }
        }

        return view('admin.events.available_online', [
            'events' => $events,
            'multisport' => 0,
        ]);

    }

    /**
     * @deprecated
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application
     */
    public function available_online_old()
    {
        $context = stream_context_create(array('http' => array('header' => 'Accept: application/xml')));
        $url = 'http://xml.cdn.betclic.com/odds_it.xml';
        $competitions = ['Champions League', 'Europa League', 'Euro U21', 'Euro 2021', 'Coppa America',
            'Coppa d\'Africa', 'Europa Conference League', 'Mondiali'];

        $xml = file_get_contents($url, false, $context);
        $xml = simplexml_load_string($xml, "SimpleXMLElement", LIBXML_NOBLANKS | LIBXML_NOBLANKS);
        $events = [];
        $matchKey = 0;

        foreach ($xml->sport as $sport) {
            if ((string)$sport->attributes()->{'name'} == 'Calcio') {
                foreach ($sport->event as $event) {
                    if (in_array((string)$event->attributes()->{'name'}, $competitions)) {

                        $matches = json_decode(json_encode($event), true)['match'];

                        foreach ($matches as $match) {

                            if (!isset($match['@attributes']['name']) && !isset($match['name'])) {
                                continue;
                            } else {
                                $name = isset($match['@attributes']['name']) ? $match['@attributes']['name'] : $match['name'];
                                $startDate = isset($match['@attributes']['start_date']) ? $match['@attributes']['start_date'] : $match['start_date'];
                            }

                            $events[] = [
                                'key' => $matchKey++,
                                'category_id' => 9,
                                'name' => $name,
                                'deadline' => Carbon::parse($startDate, 'UTC')->tz('Europe/Rome')->format('Y-m-d H:i'),
                                'competition' => (string)$event->attributes()->{'name'}
                            ];
                        }

                        if (($competitionKey = array_search((string)$event->attributes()->{'name'}, $competitions)) !== false) {
                            unset($competitions[$competitionKey]);
                        }

                        if (empty($competitions)) {
                            break;
                        }

                        continue;
                    }
                }
            }
        }

        /*foreach ($matches as $key => $match) {
            $events[] = [
                'key' => $key,
                'category_id' => 2,
                'name' => $match['@attributes']['name'],
                'deadline' => Carbon::parse($match['@attributes']['start_date'])->addHour()->format('Y-m-d H:i'),
            ];
        }*/

        return view('admin.events.available_online', [
            'events' => $events,
            'multisport' => 0,
        ]);

    }

    public function add_reloadbet_event($reloadBetEventId = null)
    {
        if ($reloadBetEventId !== null) {
            $context = stream_context_create(array('http' => array('header' => 'Accept: application/json')));
            $url = 'https://www.reloadbet85.com/api/coupons/coupons?eventId=' . $reloadBetEventId;

            $response = file_get_contents($url, false, $context);

            if ($response === false) {
                die('Errore durante il recupero dei dati dall\'API coupons');
            }

            // Decodifica del JSON
            $data = json_decode($response, true);

            if ($data === null || !isset($data['events'][0]['markets'])) {
                die('Errore durante la decodifica del JSON coupons');
            }

            $teamA = '';
            $teamB = '';

            foreach($data['events'][0]['markets'] as $market) {

                if ($market['description'] == 'Match Result') {

                    //foreach ($market['outcomes'] as $outcome) {

                    $teamA = $market['outcomes'][0]['description'];
                    $teamB = $market['outcomes'][2]['description'];

                    $choices[] = [
                        'name' => '1',
                        'odd' => $market['outcomes'][0]['price'],
                        'type' => 1
                    ];

                    $choices[] = [
                        'name' => 'X',
                        'odd' => $market['outcomes'][1]['price'],
                        'type' => 1
                    ];

                    $choices[] = [
                        'name' => '2',
                        'odd' => $market['outcomes'][2]['price'],
                        'type' => 1
                    ];
                }
                // }

                if ($market['description'] == 'Double Chance') {
                    $choices[] = [
                        'name' => '1X',
                        'odd' => $market['outcomes'][0]['price'],
                        'type' => 2
                    ];

                    $choices[] = [
                        'name' => '12',
                        'odd' => $market['outcomes'][1]['price'],
                        'type' => 2
                    ];

                    $choices[] = [
                        'name' => 'X2',
                        'odd' => $market['outcomes'][2]['price'],
                        'type' => 2
                    ];
                }

                if ($market['description'] == 'Both Teams To Score') {
                    foreach ($market['outcomes'] as $outcome) {
                        if (strtolower($outcome['description']) == 'yes') {
                            $choices[] = [
                                'name' => 'Goal',
                                'odd' => $outcome['price'],
                                'type' => 3
                            ];
                        }
                        else {
                            $choices[] = [
                                'name' => 'No Goal',
                                'odd' => $outcome['price'],
                                'type' => 3
                            ];
                        }
                    }
                }

                if ($market['description'] == 'Total Goals Over/Under') {
                    foreach ($market['outcomes'] as $outcome) {
                        if (in_array($outcome['description'], ['Over 2.5', 'Under 2.5'])) {
                            $choices[] = [
                                'name' => $outcome['description'],
                                'odd' => $outcome['price'],
                                'type' => 4
                            ];
                        }
                        //         }
                    }
                }

                if ($market['description'] == 'Correct Score') {
                    foreach ($market['outcomes'] as $outcome) {
                        $score = reloadBetExtractCorrectScore($outcome['description'], $teamA, $teamB);

                        if ($score) {
                            $choices[] = [
                                'name' => 'Risultato esatto: ' . reloadBetExtractCorrectScore($outcome['description'], $teamA, $teamB),
                                'odd' => $outcome['price'],
                                'type' => 5
                            ];
                        }
                    }

                    $choices[] = [
                        'name' => 'Risultato esatto: Altro',
                        'odd' => 0,
                        'type' => 5
                    ];
                }

                if ($market['description'] == 'Anytime GoalScorer') {
                    foreach ($market['outcomes'] as $outcome) {
                        $choices[] = [
                            'name' => 'Marcatore: '. $outcome['description'],
                            'odd' => $outcome['price'],
                            'type' => 6
                        ];
                    }
                }
            }

            /*array_multisort(array_column($choices, 'type'), SORT_ASC,
                array_column($choices, 'name'), SORT_ASC,
                $choices);*/

            usort($choices, function ($a, $b) {
                return $a['type'] <=> $b['type'];
            });

            return view('admin.events.add', [
                'categories' => Category::orderBy('id', 'DESC')->get(),
                'event' => $data['events'][0]['description'],
                'deadline' => Carbon::parse($data['events'][0]['datetime'], 'UTC')->tz('Europe/Rome')->format('Y-m-d H:i'),
                'choices' => Collect($choices)->values()->all(),
                'categoryId' => 26,
            ]);
        }

        return view('admin.events.add', [
            'categories' => Category::orderBy('id', 'DESC')->get(),
            'event' => '',
            'deadline' => '',
            'choices' => [],
            'categoryId' => null
        ]);
    }

    /**
     * @deprecated
     * @param null $key
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application
     */
    public function add_event($key = null)
    {
        if ($key !== null) {
            $context = stream_context_create(array('http' => array('header' => 'Accept: application/xml')));
            $url = 'http://xml.cdn.betclic.com/odds_it.xml';

            $xml = file_get_contents($url, false, $context);
            $xml = simplexml_load_string($xml, "SimpleXMLElement", LIBXML_NOBLANKS | LIBXML_NOBLANKS);
            $competitions = ['Champions League', 'Europa League', 'Euro U21', 'Euro 2021', 'Coppa America',
                'Coppa d\'Africa', 'Europa Conference League', 'Mondiali'];
            $matchKey = 0;

            foreach ($xml->sport as $sport) {
                if ((string)$sport->attributes()->{'name'} == 'Calcio') {
                    foreach ($sport->event as $event) {
                        if (in_array((string)$event->attributes()->{'name'}, $competitions)) {
                            $matches = json_decode(json_encode($event), true)['match'];

                            foreach ($matches as $match) {
                                $events[$matchKey++] = $match;
                            }


                            if (($competitionKey = array_search((string)$event->attributes()->{'name'}, $competitions)) !== false) {
                                unset($competitions[$competitionKey]);
                            }

                            if (empty($competitions)) {
                                break;
                            }

                            continue;
                        }
                    }
                }
            }

            $data = $events[$key];

            foreach ($data['bets'] as $key => $bets) {

                foreach ($bets as $key => $bet) {

                    $p = in_array($bet['@attributes']['name'], [
                        'Risultato esatto', 'Esito incontro',
                        'Doppia chance', 'Giocatore a segno', 'Entrambe a segno (Gol/ No gol)'
                    ]);
                    $temp = $bet['@attributes']['name'];
                    foreach ($bet['choice'] as $choice) {
                        if ($p ||
                            ($choice['@attributes']['name'] == 'Under 2.5' && $bet['@attributes']['name'] == 'Over/ Under') ||
                            ($choice['@attributes']['name'] == 'Over 2.5' && $bet['@attributes']['name'] == 'Over/ Under')) {

                            if ($temp == 'Risultato esatto') {
                                $name = 'Risultato esatto: ' . str_replace(' ', '', $choice['@attributes']['name']);
                                $type = 5;
                            } else if ($temp == 'Giocatore a segno') {
                                $name = 'Marcatore: ' . $choice['@attributes']['name'];
                                $type = 6;
                            } else if ($temp == 'Doppia chance') {
                                $name = str_replace('o', '', $choice['@attributes']['name']);
                                $name = str_replace(' ', '', $name);
                                $name = str_replace('%', '', $name);
                                $type = 2;
                            } else if ($temp == 'Risultato esatto') {
                                $name = str_replace(' ', '', $choice['@attributes']['name']);
                            } else if ($temp == 'Entrambe a segno (Gol/ No gol)') {
                                $name = str_replace('Sì', 'Goal', $choice['@attributes']['name']);
                                $name = str_replace('No', 'No Goal', $name);
                                $type = 3;
                            } else {
                                $name = str_replace('%', '', $choice['@attributes']['name']);
                                if ($bet['@attributes']['name'] == 'Over/ Under') {
                                    $type = 4;
                                } else {
                                    $type = 1;
                                }
                            }

                            $choices[] = [
                                'name' => $name,
                                'odd' => $choice['@attributes']['odd'],
                                'type' => $type
                            ];
                        }
                    }
                }
            }

            array_multisort(array_column($choices, 'type'), SORT_ASC,
                array_column($choices, 'name'), SORT_ASC,
                $choices);

            return view('admin.events.add', [
                'categories' => Category::orderBy('id', 'DESC')->get(),
                'event' => $data['@attributes']['name'],
                'deadline' => Carbon::parse($data['@attributes']['start_date'], 'UTC')->tz('Europe/Rome')->format('Y-m-d H:i'),
                'choices' => Collect($choices)->values()->all(),
                'categoryId' => 26,
            ]);
        }

        return view('admin.events.add', [
            'categories' => Category::orderBy('id', 'DESC')->get(),
            'event' => '',
            'deadline' => '',
            'choices' => [],
            'categoryId' => null
        ]);


    }

    public function add_by_file($filename)
    {
        $contents = explode("\n", file_get_contents(storage_path($filename)));

        return view('admin.events.add_by_file', [
            'categories' => Category::orderBy('id', 'DESC')->get(),
            'contents' => $contents,
            'categoryId' => 26,
        ]);
    }

    public function add_multisport($sportId = null, $key = null)
    {
        if ($key !== null) {
            $context = stream_context_create(array('http' => array('header' => 'Accept: application/xml')));
            $url = 'http://xml.cdn.betclic.com/odds_it.xml';

            $xml = file_get_contents($url, false, $context);
            $xml = simplexml_load_string($xml, "SimpleXMLElement", LIBXML_NOBLANKS | LIBXML_NOBLANKS);
            $competitions = ['Europa League', 'Euro U21', 'Euro 2021', 'Coppa America', 'Mondiali'];
            $name = '';
            $events = [];

            foreach ($xml->sport as $sport) {
                // if ((string)$sport->attributes()->{'name'} == 'Calcio') {
                $matchKey = 0;
                //  dump($sport);
                foreach ($sport->event as $event) {

                    $eventName = $event->attributes()->{'name'};

                    $s = (array)$sport->attributes()->{'id'};

                    //  dump($name);
                    //  dump((string)strtolower($eventName));
                    if ((str_contains((string)strtolower($eventName), 'olimpiadi') || str_contains((string)strtolower($eventName), 'parigi') || $sport->attributes()->{'name'} == 'Speciali Olimpiadi'
                            || (str_contains((string)strtolower($eventName), 'olympic')) || (str_contains((string)strtolower($eventName), 'paris')))
                        && $s[0] == $sportId) {

                        $matches = $event->children();

                        foreach ($matches as $match) {
                            if ($matchKey++ == $key) {
                                $name .= ($sport->attributes()->{'name'} . ' - ' . $event->attributes()->{'name'});
                                $events = $match;
                            }
                        }


                        if (($competitionKey = array_search((string)$event->attributes()->{'name'}, $competitions)) !== false) {
                            unset($competitions[$competitionKey]);
                        }

                        if (empty($competitions)) {
                            break;
                        }

                        continue;
                    }
                }
                //     }
            }
            //  exit();
            $data = (array)$events;

            if (isset($data['bets'])) {
                $betsTemp = (array)$data['bets'];
                foreach ($betsTemp as $bets) {

                    if (is_array($bets)) {
                        $bets = $bets[0];
                    }

                    $name .= (' - ' . $bets->attributes()->{'name'});
                    $type = 1;

                    foreach ($bets->children() as $choice) {

                        $choices[] = [
                            'name' => (string)$choice->attributes()->name,
                            'odd' => (string)$choice->attributes()->odd,
                            'type' => $type
                        ];
                    }
                }
            }
            if (isset($data['bet'])) {
                $bets = $data['bet'];

                //foreach ($bets as $key => $bet) {
                /*  $p = in_array($bet['@attributes']['name'], [
                      'Risultato esatto', 'Esito incontro',
                      'Doppia chance', 'Giocatore a segno', 'Entrambe a segno (Gol/ No gol)'
                  ]);*/
                //dd($bet);
                // dd($bets);
                $name .= (' - ' . ($bets['@attributes']['name']));
                $type = 1;

                foreach ($bets['choice'] as $choice) {

                    $choices[] = [
                        'name' => $choice->attributes()->{'name'}[0],
                        'odd' => $choice->attributes()->{'odd'}[0],
                        'type' => $type
                    ];
                }
            }

            array_multisort(array_column($choices, 'type'), SORT_ASC,
                array_column($choices, 'name'), SORT_ASC,
                $choices);

            $deadline = Carbon::parse('2024-07-26 23:59:00');
            /* if ( isset($data['@attributes']['start_date'])) {
                 $deadline = Carbon::parse($data['@attributes']['start_date'])->addHour(2)->format('Y-m-d H:i');
         }*/

            return view('admin.events.add', [
                'categories' => Category::all(),
                'event' => $name,
                'deadline' => $deadline,
                'choices' => Collect($choices)->values()->all(),
                'categoryId' => 25,
            ]);
        }

        return view('admin.events.add', [
            'categories' => Category::all(),
            'event' => '',
            'deadline' => '',
            'choices' => [],
            'categoryId' => null
        ]);


    }

    public function store(Request $request)
    {
        // validate
        $this->validate($request, [
            'category_id' => 'required|numeric',
            'name' => 'required|string',
            'deadline' => 'required|date'
        ]);

        $event = new Event([
            'category_id' => $request->input('category_id'),
            'name' => $request->input('name'),
            'deadline' => $request->input('deadline'),
            'private' => empty($request->input('private')) ? 0 : 1,
        ]);

        $event->save();

        foreach ($request->input('bet_name') as $index => $value) {
            if (isset($request->input('bet_points')[$index])) {
                $bet = new Bet([
                    'event_id' => $event->id,
                    'name' => $value,
                    'points' => $request->input('bet_points')[$index],
                ]);
                $bet->save();
            }
        }

        Session::flash('message', trans('custom.event_success'));

        return redirect(route('admin.event.available_online'));
    }
}
