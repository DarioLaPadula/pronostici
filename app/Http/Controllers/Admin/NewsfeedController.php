<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\NewsfeedRequest;
use App\Newsfeed;
use App\Tournament;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NewsfeedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.newsfeeds.index',[
            'newsfeeds' => Newsfeed::orderBy('id', 'DESC')->get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.newsfeeds.add', [
            'tournaments' => Tournament::orderBy('id','DESC')->get(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NewsfeedRequest $request)
    {
        Newsfeed::create(array_merge($request->all(), ['user_id' => Auth::user()->id]));

        return response()->redirectToRoute('admin.newsfeeds.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Newsfeed $Newsfeed
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Newsfeed $Newsfeed)
    {
        /*return view('common.Newsfeeds.edit', [
            'Newsfeed' => $Newsfeed,
            'Newsfeeds' => Newsfeed::all(),
        ]);*/
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Lesson  $lesson
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Newsfeed $Newsfeed)
    {
        /*$validatedData = $this->NewsfeedPresentation->validateUpdate($request->all());

        if ($validatedData->fails()) {
            $this->flashError('errore');
            return response()->redirectToRoute('admin.Newsfeeds.edit', ['Newsfeed_id' => $Newsfeed->id])
                ->withErrors($validatedData)
                ->withInput();
        }

        $Newsfeed->fill($request->all())->save();

        return response()->redirectToRoute('admin.Newsfeeds.index');*/
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Newsfeed  $Newsfeed
     * @return \Illuminate\Http\Response
     */
    public function destroy(Newsfeed $newsfeed)
    {
        $newsfeed->delete();
        return response()->redirectToRoute('admin.newsfeeds.index');
    }
}
