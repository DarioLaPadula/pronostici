<?php namespace App\Http\Controllers\AjaxController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AjaxBaseController extends Controller
{
    protected function response_composer($status, $message, $data = null)
    {
        return array('status' => $status, 'message' => $message, 'data' => $data);
    }
}
