<?php namespace App\Http\Controllers\AjaxController;

use App\User;
use File;
use Illuminate\Http\Request;
use Validator;
use Auth;
use Lang;

class AjaxUserController extends AjaxBaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function removeAvatar(Request $request) {
        $user = User::find($request->input('user_id'));
        if ($user && $user->avatar != config('custom.default_user_image')) {
            File::delete(public_path('storage/uploads/avatars/') . $user->avatar);
            $user->fill(['avatar' => config('custom.default_user_image')])->save();
            return $this->response_composer(true, '');
        }
        return $this->response_composer(false, '');
    }
}
