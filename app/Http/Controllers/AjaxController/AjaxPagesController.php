<?php namespace App\Http\Controllers\AjaxController;

use App\Tournament;
use App\TournamentUser;
use Illuminate\Http\Request;
use Auth;

class AjaxPagesController extends AjaxBaseController
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * 
     *
     * @return \Illuminate\Http\Response
     */
    public function store_tournament_user(Request $request)
    {
        // Check if tournament is available (deadline not reached)
        $tournamentAvailable = Tournament::where($request->input('tournament_id'))
            ->isAvailable()
            ->count();

        if (!$tournamentAvailable) {
            return $this->response_composer(false, trans('custom.tournament_not_available'));
        }

        // @TODO: Vedere se c'è metodo migliore (rules o qualcosa del genere)
        $tournamentUser = TournamentUser::where([
                'tournament_id' => $request->input('tournament_id'),
                'user_id' => Auth::user()->id,
            ])
            ->first();

        // tournament_user already exists
        if ($tournamentUser) {
            return $this->response_composer(false, trans('custom.user_already_signed'));
        }

        $tu = new TournamentUser([
            'tournament_id' => $request->input('tournament_id'),
            'user_id' => Auth::user()->id,
        ]);

        if ($tu->save()) {
            return $this->response_composer(true,  trans('custom.subscribed'));
        }
        return $this->response_composer(false, trans('custom.error_occurred'));
       
    }
}
