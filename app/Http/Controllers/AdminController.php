<?php namespace App\Http\Controllers;

use App\Group;
use App\GroupUser;
use Illuminate\Http\Request;
use App\Category;
use App\Event;
use App\Result;
use App\Point;
use App\User;
use App\Tournament;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Carbon;
use Auth;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.admin');
    }

    public function calculate()
    {
        $events = Event::where('calculated', 0)
            ->with(['bet' => function ($query) {
                $query->with('results')
                    ->whereHas('results', function ($query) {
                        $query->where('name', 'like', 'Marcatore: %');
                    });
            }])
            ->orderBy('id', 'asc')
            ->get();

        $calculated = Event::where('calculated', 1)
            ->where('updated_at', '>=', Carbon::now()->subMonth())
            ->orderBy('id', 'desc')
            ->get();

        return view('admin.calculate', [
            'events' => $events,
            'calculated' => $calculated,
        ]);
    }

    public function postcalculate(Request $request)
    {
        // validate
        $this->validate($request, [
            'event_id' => 'required|numeric|exists:events,id',
            'result' => 'required|string|regex:/^\d+-\d+$/',
            'scorer_ids' => 'nullable|array|exists:bets,id,event_id,'.$request->event_id,
            'final_summary' => 'nullable|string',
        ]);

        $event = Event::where(['id' => $request->input('event_id')])->first();

        $win = $this->__calculateResults($request->input('result'));

        $results = Result::where([
            'event_id' => $request->input('event_id'),
            'win' => 0,
        ])
            ->with('Bet')
            ->get();

        foreach ($results as $result) {
            $bet = trim(strtolower(str_replace('Risultato esatto: ', '', $result->bet->name)));

            if (in_array($bet, $win) || ($request->has('scorer_ids') && in_array($result->bet->id, $request->input('scorer_ids')))) {
                $result->win = 1;
                $this->__calculatePoints($result->user_id, $result->bet->points, $event->category_id);
            } else {
                $result->win = -1;
            }
            $result->save();
        }

        $event->calculated = 1;
        $event->final_summary = $request->input('final_summary');
        $event->save();

        return array('status' => 'success');
    }

    public function undocalculate(Request $request)
    {
        // validate
        $this->validate($request, [
            'event_id' => 'required|numeric|exists:events,id',
        ]);

        $event = Event::where(['id' => $request->input('event_id')])->first();

        $results = Result::where([
            'event_id' => $request->input('event_id'),
        ])
          //  ->with('Bet')
            ->get();

        foreach ($results as $result) {
            $result->win = 0;
            $this->__undoPoints($result->user_id, $result->bet->points, $event->category_id);
            $result->save();
        }

        $event->calculated = 0;
        $event->final_summary = null;
        $event->save();

        return array('status' => 'success');
    }

    public function draw_group(Tournament $tournament)
    {
        $users = User::select('users.id', 'users.name')
            ->join('tournaments_users', 'tournaments_users.user_id', 'users.id')
            ->where('tournaments_users.tournament_id', $tournament->id)
            ->orderBy('name', 'asc')
            ->get();

        $res1 = $users->pluck('id')->toArray();

        $usersPoints = User::select('users.id', 'users.name', 'points.points')
            ->join('points', 'points.user_id', 'users.id')
            ->whereIn('users.id', $res1)
            ->where('points.tournament_id', '>=', 16)
            ->groupBy('users.id')
            ->orderBy('points.points', 'desc')
            ->get();

        $a = $usersPoints->toArray();

        //dd($users->pluck('id')->toArray());

        // dd(count($a));

        // Fascia 1
        foreach($a as $key => $userDraw) {
            if ($key <= 32) {
                $res[$key/8][] = $userDraw['id'];
            }
            else {
                $res[4][] = $userDraw['id'];
            }
            if (($kk = array_search($userDraw['id'], $res1)) !== false) {
                unset($res1[$kk]);
            }
        }

        foreach($res1 as $r) {
            $res[4][] = $r;
        }

        foreach ($users as $user) {
            // echo "'" . $user->id . "', ";
            //  continue;
            /*[62,199,285,1,200,327,3,75]
            [324,248,33,343,10,312,284,34]
[26,24,95,139,75,22,4,344]
[2,139,334,32,233,52,88,311]*/
            // ()
            /* if (in_array($user->id, [62,199,285,1,200,327,3,75])) {
                 $res[9][] = $user->id;
                 //   $res['Fascia Champions 2021/22'][] = $user->id;
             }
             else if (in_array($user->id, [324,248,33,343,10,312,284,34])) {
                 $res[6][] = $user->id;
                 // $res['Fascia Europa League 2021'][] = $user->id;
             }
             else if (in_array($user->id, [26,24,95,139,22,4,344,2])) {
                 $res[3][] = $user->id;
                 //  $res['Fascia Pechino 2022'][] = $user->id;
             }
             else if (in_array($user->id, [334,32,233,245,52,88,41,43])) {
                 $res[0][] = $user->id;
                 //  $res['Coppa d\'Africa 2022'][] = $user->id;
             }*/
            /*if (in_array($user->id, [4, 26, 41, 385])) {
                $res[2][] = $user->id;
                //   $res['Fascia Champions 2021/22'][] = $user->id;
            } else if (in_array($user->id, ['33', '43', '64', '401',
            ])) {
                $res[0][] = $user->id;
                // $res['Fascia Europa League 2021'][] = $user->id;
            }*/ /*else if (in_array($user->id, [10, 62, 284, 285, 334, 343, 363, 381])) {
                $res[1][] = $user->id;
                //  $res['Fascia Pechino 2022'][] = $user->id;
            } else if (in_array($user->id, [1, 2, 4, 30, 41, 245, 361, 378])) {
                $res[0][] = $user->id;
                //  $res['Coppa d\'Africa 2022'][] = $user->id;
            }*/

            //else {
            // $res[0][] = $user->id;
            //$res['Altri'][] = $user->name;
            //}
        }

        //exit();
        // ksort($res);
        // dd($res);

        //$groups = ['A', 'B', 'C', 'D'];

        $groups = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'];
        $savedGroups = [];

        foreach ($groups as $group) {
            $savedGroups[] = Group::create([
                'phase_id' => 60,
                'group' => $group
            ]);
        }

        $index = 0;
        foreach (array_reverse($res,true) as $key => $pot) {
            shuffle($pot);
            foreach ($pot as $user) {
                GroupUser::create([
                    //'group_id' => $savedGroups[$index % 4]->id,
                    'group_id' => $savedGroups[$index % 8]->id,
                    'user_id' => $user,
                    'bonus' => 0,
                ]);
                $index++;
                unset($pot[$key]);
            }
        }

        return 'ok';

        //dd($res);

        /* $users = [
             'Fascia Champions 2020/21' => ['Fabio Prencipe', 'FraOrru', 'Dario La Padula', 'Manolo', 'Luca Pacella', 'Gianmarco Corsi', 'Francesco Monopoli', 'Danilo C'],

             'Fascia Euro 2020:' => ['Claudio Massarotti', 'Massum', 'Camilla Galitzia', 'Andrea Trapani', 'AleGM93', 'Mirko P', 'Antonino Falsone', 'Daniele Sangermano'],

             'Fascia Euro U21 2020 / Tokyo 2020:' => ['AleR', 'Salvatore Senarcia', 'FVG', 'Tommy S', 'LucaMP', 'Mike', 'Francesco Moi', 'Morris Falco'],

             'Fascia Europa League 2021:' => ['Fabrizio Di Coste', 'luigi campese', 'Alessandro Zaffini', 'Andrea Ganci', 'Liezmig', 'Ludovica Ciufoli', 'Federico Fadda', 'Andrea81'],

             'Altri:' => ['Alessandro Zincone', 'Maurizio D\'Andrea', 'Brucaliffo', 'Francesco Brate', 'Hermann Thierry Marquiand', 'sMartys',
                 'Federico Zafalon', 'Momen', 'Martina Grassi', 'Matteo Accarrino', 'Hamza', 'Giacomo Carnali', 'Timur Glazkov', 'Sealo33', 'Jacopo Guastella', 'Federico Ciufoli'],
         ];*/

        return view('admin.draw', ['users' => $res]);
    }

    public function draw_second_phase(Tournament $tournament)
    {
        $users = User::select('users.id', 'users.name')
            ->join('tournaments_users', 'tournaments_users.user_id', 'users.id')
            ->where('tournaments_users.tournament_id', $tournament->id)
            ->orderBy('name', 'asc')
            ->get();

        $res = [];

        foreach ($users as $user) {
            if (in_array($user->id, ['344','284','26','334','24','402','401','450'
            ])) {
                $res[2][] = $user->id;
            }
            if (in_array($user->id, ['14','41','312','383','418','95','381','285'
            ])) {
                $res[0][] = $user->id;
                //   $res['Fascia Champions 2021/22'][] = $user->id;
            } /*else if (in_array($user->id, ['24',
                '26',
                '33',
                '285',
                '312',
                '334',
                '385',
                '402',
            ])) {
                $res[0][] = $user->id;
            }*/
        }

        $groups = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'];
        $savedGroups = [];

        foreach ($groups as $group) {
            $savedGroups[] = Group::create([
                'phase_id' => 71,
                'group' => $group
            ]);
        }

        $index = 0;
        foreach (array_reverse($res,true) as $key => $pot) {
            shuffle($pot);
            foreach ($pot as $user) {
                GroupUser::create([
                    'group_id' => $savedGroups[$index % 8]->id,
                    //'group_id' => $savedGroups[$index % 8]->id,
                    'user_id' => $user,
                    'bonus' => $key,
                ]);
                $index++;
                unset($pot[$key]);
            }
        }

        return 'ok';

        //dd($res);

        /* $users = [
             'Fascia Champions 2020/21' => ['Fabio Prencipe', 'FraOrru', 'Dario La Padula', 'Manolo', 'Luca Pacella', 'Gianmarco Corsi', 'Francesco Monopoli', 'Danilo C'],

             'Fascia Euro 2020:' => ['Claudio Massarotti', 'Massum', 'Camilla Galitzia', 'Andrea Trapani', 'AleGM93', 'Mirko P', 'Antonino Falsone', 'Daniele Sangermano'],

             'Fascia Euro U21 2020 / Tokyo 2020:' => ['AleR', 'Salvatore Senarcia', 'FVG', 'Tommy S', 'LucaMP', 'Mike', 'Francesco Moi', 'Morris Falco'],

             'Fascia Europa League 2021:' => ['Fabrizio Di Coste', 'luigi campese', 'Alessandro Zaffini', 'Andrea Ganci', 'Liezmig', 'Ludovica Ciufoli', 'Federico Fadda', 'Andrea81'],

             'Altri:' => ['Alessandro Zincone', 'Maurizio D\'Andrea', 'Brucaliffo', 'Francesco Brate', 'Hermann Thierry Marquiand', 'sMartys',
                 'Federico Zafalon', 'Momen', 'Martina Grassi', 'Matteo Accarrino', 'Hamza', 'Giacomo Carnali', 'Timur Glazkov', 'Sealo33', 'Jacopo Guastella', 'Federico Ciufoli'],
         ];*/

        return view('admin.draw', ['users' => $res]);
    }

    public function draw(Tournament $tournament)
    {
        $users = User::select('users.id', 'users.name')
            ->join('tournaments_users', 'tournaments_users.user_id', 'users.id')
            ->where('tournaments_users.tournament_id', $tournament->id)
            ->orderBy('name', 'asc')
            ->get();

        $champions = User::select('users.id', 'users.name')
            ->join('tournaments_users', 'tournaments_users.user_id', 'users.id')
            ->join('points', function ($join) {
                 $join->on('points.user_id', '=', 'users.id')
                     ->where('points.tournament_id', 20);
             })
            ->where('tournaments_users.tournament_id', $tournament->id)
            ->orderBy('points', 'desc')
            ->limit(8)
            ->get();

        $champions_ids = Collect($champions)->pluck('id')->toArray();

        $europaLeague = User::select('users.id', 'users.name')
            ->join('tournaments_users', 'tournaments_users.user_id', 'users.id')
            ->join('points', function ($join) {
                $join->on('points.user_id', '=', 'users.id')
                    ->where('points.tournament_id', 21);
            })
            ->where('tournaments_users.tournament_id', $tournament->id)
            ->whereNotIn('users.id', $champions_ids)
            ->orderBy('points', 'desc')
            ->limit(8)
            ->get();

        $el_ids = Collect($europaLeague)->pluck('id')->toArray();

        $africa = User::select('users.id', 'users.name')
            ->join('tournaments_users', 'tournaments_users.user_id', 'users.id')
            ->join('points', function ($join) {
                $join->on('points.user_id', '=', 'users.id')
                    ->where('points.tournament_id', 22);
            })
            ->where('tournaments_users.tournament_id', $tournament->id)
            ->whereNotIn('users.id', array_merge($champions_ids, $el_ids))
            ->orderBy('points', 'desc')
            ->limit(8)
            ->get();

        $africa_ids = Collect($africa)->pluck('id')->toArray();

        $others = User::select('users.id', 'users.name')
            ->join('tournaments_users', 'tournaments_users.user_id', 'users.id')
            ->where('tournaments_users.tournament_id', $tournament->id)
            ->whereNotIn('users.id', array_merge($champions_ids, $el_ids, $africa_ids))
            ->orderBy('name', 'asc')
            ->get();

        $others_ids = Collect($others)->pluck('id')->toArray();

        $groups = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'];
        $savedGroups = [];

        $res[0] = $champions_ids;
        $res[1] = $el_ids;
        $res[2] = $africa_ids;
        $res[3] = $others_ids;

        foreach ($groups as $group) {
            $savedGroups[] = Group::create([
                'phase_id' => 70,
                'group' => $group
            ]);
        }

        $index = 0;
        foreach ($res as $users) {
            shuffle($users);
            foreach ($users as $user) {
                GroupUser::create([
                    //'group_id' => $savedGroups[$index % 4]->id,
                    'group_id' => $savedGroups[$index % 8]->id,
                    'user_id' => $user,
                    'bonus' => 0,
                ]);
                $index++;
                //unset($users[$key]);
            }
        }

        return 'ok';

        //dd($res);

        /*$users = [
             'Fascia Champions 2020/21' => ['Fabio Prencipe', 'FraOrru', 'Dario La Padula', 'Manolo', 'Luca Pacella', 'Gianmarco Corsi', 'Francesco Monopoli', 'Danilo C'],

             'Fascia Euro 2020:' => ['Claudio Massarotti', 'Massum', 'Camilla Galitzia', 'Andrea Trapani', 'AleGM93', 'Mirko P', 'Antonino Falsone', 'Daniele Sangermano'],

             'Fascia Euro U21 2020 / Tokyo 2020:' => ['AleR', 'Salvatore Senarcia', 'FVG', 'Tommy S', 'LucaMP', 'Mike', 'Francesco Moi', 'Morris Falco'],

             'Fascia Europa League 2021:' => ['Fabrizio Di Coste', 'luigi campese', 'Alessandro Zaffini', 'Andrea Ganci', 'Liezmig', 'Ludovica Ciufoli', 'Federico Fadda', 'Andrea81'],

             'Altri:' => ['Alessandro Zincone', 'Maurizio D\'Andrea', 'Brucaliffo', 'Francesco Brate', 'Hermann Thierry Marquiand', 'sMartys',
                 'Federico Zafalon', 'Momen', 'Martina Grassi', 'Matteo Accarrino', 'Hamza', 'Giacomo Carnali', 'Timur Glazkov', 'Sealo33', 'Jacopo Guastella', 'Federico Ciufoli'],
         ];*/

        return view('admin.draw', ['users' => $res]);
    }

    public function draw2024(Tournament $tournament)
    {
        $users = User::select('users.id', 'users.name')
            ->join('tournaments_users', 'tournaments_users.user_id', 'users.id')
            ->where('tournaments_users.tournament_id', $tournament->id)
            ->orderBy('name', 'asc')
            ->get();

            $savedGroup = Group::create([
                'phase_id' => 75,
                'group' => 'Girone Unico',
            ]);

            foreach ($users as $user) {
                GroupUser::create([
                    //'group_id' => $savedGroups[$index % 4]->id,
                    'group_id' => $savedGroup->id,
                    'user_id' => $user->id,
                    'bonus' => 0,
                ]);
            }

        return 'ok';


        return view('admin.draw', ['users' => $res]);
    }

    private function __calculateResults($input)
    {
        $win = [];
        $results = explode(',', $input);

        foreach ($results as $result) {
            $result = str_replace(['Marcatore: ', 'Risultato esatto: '], ['', ''], $result);

            if (strpos($result, '-') !== false) {
                // Inserito Risultato esatto
                $temp = explode('-', $result);
                $home = intval($temp[0]);
                $away = intval($temp[1]);

                // 1
                if ($home > $away) {
                    $win[] = '1';
                    $win[] = '1x';
                    $win[] = '12';
                }
                // X
                if ($home == $away) {
                    $win[] = 'x';
                    $win[] = '1x';
                    $win[] = 'x2';
                }
                // 2
                if ($home < $away) {
                    $win[] = '2';
                    $win[] = 'x2';
                    $win[] = '12';
                }

                // Goal
                if (($home > 0) && ($away > 0)) {
                    $win[] = 'goal';
                    $win[] = 'gol';
                }
                // No Goal
                if (($home == 0) || ($away == 0)) {
                    $win[] = 'no goal';
                    $win[] = 'no gol';
                }
                // Over 2.5
                if ($home + $away > 2.5) {
                    $win[] = 'over 2.5';
                    $win[] = 'over 2,5';
                }
                // Under 2.5
                if ($home + $away < 2.5) {
                    $win[] = 'under 2.5';
                    $win[] = 'under 2,5';
                }
                // Altro
                if ($home > 4 || $away > 4) {
                    $win[] = 'altro';
                }
            }
            $win[] = trim(strtolower($result));
        }

        return $win;
    }

    private function __calculatePoints($userId, $points, $categoryId)
    {
        $tournaments = Tournament::where([
            'category_id' => $categoryId
        ])->get();

        foreach ($tournaments as $tournament) {

            $point = Point::where([
                'user_id' => $userId,
                'tournament_id' => $tournament->id
            ])
                ->first();
            // Controllo se point già esiste
            if ($point) {
                $tempPoints = $point->points + $points;
                $point->points = number_format($tempPoints, 2);
            } else {
                $point = new point([
                    'tournament_id' => $tournament->id,
                    'user_id' => $userId,
                    'points' => number_format($points, 2),
                    'last_update' => Carbon::now()
                ]);
            }

            $point->save();
        }
    }

    private function __undoPoints($userId, $points, $categoryId)
    {
        $tournaments = Tournament::where([
            'category_id' => $categoryId
        ])->get();

        foreach ($tournaments as $tournament) {

            $point = Point::where([
                'user_id' => $userId,
                'tournament_id' => $tournament->id
            ])
                ->first();

            // Controllo se point già esiste
            if ($point) {
                $tempPoints = $point->points - $points;
                $point->points = number_format($tempPoints, 2);
                $point->save();
            }
        }
    }

    public function add_category()
    {
        return view('admin.add_category');
    }

    public function store_category(Request $request)
    {
        // validate
        $this->validate($request, [
            'name' => 'required|string',
        ]);

        $category = new Category([
            'name' => $request->input('name'),
        ]);

        $category->save();

        Session::flash('message', trans('custom.category_added'));

        return redirect(route('admin.categories.add'));
    }

    public function list()
    {
        return view('admin.add_tournament', [
            'categories' => Category::all()
        ]);
    }

    public function cacheClear()
    {
        \Artisan::call('config:clear');
        \Artisan::call('cache:clear');
        \Artisan::call('config:cache');
        return redirect(route('pages.index'));
    }

    /*$data = $events[$key][$key];

    foreach ($data as $key => $bets) {

    foreach ($bets['bets'] as $key => $bet) {
    foreach ($bet as $b) {
    $temp = $b['@attributes']['name'];
    $p = in_array($b['@attributes']['name'], [
    'Risultato esatto', 'Esito incontro',
    'Doppia chance', 'Giocatore a segno', 'Entrambe a segno (Gol/ No gol)'
    ]);
    foreach ($b['choice'] as $c) {

        //  foreach ($b['choice'] as $c) {
    if ($temp == 'Risultato esatto') {
    $name = 'Risultato esatto: ' . str_replace(' ', '', $c['@attributes']['name']);
    $type = 5;
    } else if ($temp == 'Giocatore a segno') {
        $name = 'Marcatore: ' . $c['@attributes']['name'];
        $type = 6;
    } else if ($temp == 'Doppia chance') {
        $name = str_replace('o', '', $c['@attributes']['name']);
        $name = str_replace(' ', '', $name);
        $name = str_replace('%', '', $name);
        $type = 2;
    } else if ($temp == 'Risultato esatto') {
        $name = str_replace(' ', '', $c['@attributes']['name']);
    } else if ($temp == 'Entrambe a segno (Gol/ No gol)') {
        $name = str_replace('Sì', 'Goal', $c['@attributes']['name']);
        $name = str_replace('No', 'No Goal', $name);
        $type = 3;
    } else {
        $name = str_replace('%', '', $c['@attributes']['name']);
        if ($temp == 'Over/ Under') {
            $type = 4;
        } else {
            $type = 1;
        }
    }

    $choices[] = [
        'name' => $name,
        'odd' => $c['@attributes']['odd'],
        'type' => $type
    ];
    }
    }
    }
    }

    array_multisort(array_column($choices, 'type'), SORT_ASC,
        array_column($choices, 'name'), SORT_ASC,
        $choices);*/
}
