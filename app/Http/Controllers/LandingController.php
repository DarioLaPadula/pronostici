<?php
namespace App\Http\Controllers;

use App\Event;
use App\Mail\MailContact;
use App\Newsfeed;
use App\Result;
use App\Tournament;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Validator;
use App\User;
use Auth;
use Lang;

class LandingController extends Controller
{

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (auth()->check()) {
                return redirect(route('pages.index'));
            }
            return $next($request);
        });
    }
    public function index() {
        $lastTournament = Newsfeed::where('is_regulation', 1)
            ->orderByDesc('id')
            ->first();

        $userIp = request()->ip(); // Ottieni l'IP dell'utente
        $canSendMessage = !Cache::has("contact_message_{$userIp}");

        return view('landing.index', [
            'users' => User::count(),
            'events' => Event::count(),
            'results' => Result::count(),
            'tournaments' => Tournament::count(),
            'lastTournament' => $lastTournament,
            'canSendMessage' => $canSendMessage,
        ]);
    }


    public function rules()
    {
        $lastTournament = Newsfeed::where('is_regulation', 1)
            ->orderByDesc('id')
            ->first();

        return view('landing.rules', compact('lastTournament'));
    }

    public function send_contact_email(Request $request)
    {
        // Validazione dei dati
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|email|max:255',
            'message' => 'required|string|max:2000',
            'full_name' => 'nullable|string|in:' // Deve essere vuoto -> antispam
        ]);

        // Identifica l'utente tramite IP
        $userIp = $request->ip();
        $cacheKey = "contact_message_{$userIp}";

        // Controlla il limite giornaliero
        if (Cache::has($cacheKey)) {
            return response()->json([
                'status' => 'error'
            ], 429);
        }

        $formStartTime = $request->input('form_start_time');
        if (now()->timestamp - $formStartTime < 10) {
            Log::info('Form submitted too quickly. Possible bot.');
            Log::info($request->header('User-Agent'));
            return response()->json([
                'status' => 'error',
                'message' => 'Form submitted too quickly. Possible bot.'
            ], 422);
        }

        $data = $request->all();
        $data['subject'] = 'Messaggio da modulo landing';
        $data['ip'] = $userIp;

        // Invia la mail
        Mail::to(config('mail.from.address'))->send(new MailContact($data));

        // Imposta il limite (24 ore)
        Cache::put($cacheKey, true, now()->addDay());

        return response()->json([
            'status' => 'success'
        ], 200);
    }
}
