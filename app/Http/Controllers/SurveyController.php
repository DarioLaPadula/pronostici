<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Auth;
use App\SurveyResult;

class SurveyController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            Log::stack(['access'])->info(Auth::user()->name . ' ' . Route::getCurrentRoute()->getActionName());

            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function survey(Request $request)
    {
        foreach ($request->input('survey_result') as $index => $value) {
            $result = new SurveyResult([
                'choice_id' => $index,
                'user_id' => Auth::user()->id,
            ]);

            $result->save();
        }
        return back();
    }
}
