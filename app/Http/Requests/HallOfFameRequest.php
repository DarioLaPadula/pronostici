<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HallOfFameRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tournament_id' => 'required|integer|exists:tournaments,id',
            'first_user_ids' => 'nullable|array',
            'second_user_ids' => 'nullable|array',
            'third_user_ids' => 'nullable|array',
            'first_points_user_ids' => 'nullable|array',
            'total_points_first' => 'nullable|numeric',
            'second_points_user_ids' => 'nullable|array',
            'total_points_second' => 'nullable|numeric',
            'third_points_user_ids' => 'nullable|array',
            'total_points_third' => 'nullable|numeric',
            'best_odd_win_user_ids' => 'nullable|array',
            'best_odd_win' => 'nullable|string',
            'high_prediction_win_user_ids' => 'nullable|array',
            'total_high_prediction_win' => 'nullable|numeric',
        ];
    }
}
