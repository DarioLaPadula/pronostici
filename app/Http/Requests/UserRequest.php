<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|string|max:255|unique:users,name,'.auth()->user()->id,
            'email' => 'required|email|max:255|unique:users,email,'.auth()->user()->id,
            'password' => 'required|string|confirmed|min:5',
            'avatar' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];

        // If update, password change is not required
        if ($this->method() == 'PUT') {
            $rules['email'] = 'email|min:5';
            $rules['password'] = 'nullable|confirmed|min:5';
        }

        return $rules;
    }
}
