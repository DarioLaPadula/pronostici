<?php

namespace App\Providers;

use App\Tournament;
use App\User;
use Carbon\Carbon;
use Auth;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use JeroenNoten\LaravelAdminLte\Events\BuildingMenu;
use JeroenNoten\LaravelAdminLte\Http\ViewComposers\AdminLteComposer;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\Event' => [
            'App\Listeners\EventListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
            Event::listen(BuildingMenu::class, function (BuildingMenu $event) {

                $user = User::where('id', '=', Auth::user()->id)
                    ->with('Tournament')
                    ->first();

                $event->menu->add([
                    'text' => __('menu.dashboard'),
                    'url' => 'auth/index',
                    'icon' => 'fas fa-fw fa-table',
                ]);

                $tournamentsAvailable = Tournament::isAvailable()->get();

                if (!$tournamentsAvailable->isEmpty()) {

                    $count = Tournament::getAvailable(false)->count();

                    $add = [
                        'text' => __('menu.tournaments_available'),
                        'url' => 'auth/tournaments_available',
                        // 'label' => $count > 0 ? $count : '',
                        'icon' => 'fas fa-fw fa-clipboard-check',
                    ];

                    if ($count) {
                        $add['label'] = $count;
                    }

                    $event->menu->add($add);
                }

                // Tournament in evidence until deadline
                $userTournamentsIds = $user->tournament
                    ->where('deadline', '>=', Carbon::parse('now')->format('Y-m-d'))
                    ->pluck('id')
                    ->toArray();

                $tournaments = Tournament::where('deadline', '>=', Carbon::parse('now')->format('Y-m-d'))
                    ->orderByDesc('id')
                    ->get();

                if ($tournaments) {
                    foreach ($tournaments as $tournament) {
                        // Costruisci il sottomenu

                        $submenu = [
                            [
                                'text' => __('menu.news'),
                                'url' => 'auth/dashboard/' . $tournament->id,
                                'icon' => 'fas fa-fw fa-newspaper',
                            ],
                            [
                                'text' => __('menu.add_results'),
                                'url' => 'auth/add/' . $tournament->id,
                                'icon' => 'fas fa-pencil-alt',
                                'visible' => in_array($tournament->id, $userTournamentsIds), // Condizione di visibilità
                            ],
                            [
                                'text' => __('menu.list_results'),
                                'url' => 'auth/show/' . $tournament->id,
                                'icon' => 'fa fa-clipboard-list',
                            ],
                            [
                                'text' => __('menu.my_results'),
                                'url' => 'auth/results/' . $user->id . '/' . $tournament->id,
                                'icon' => 'fa fa-address-book',
                                'visible' => in_array($tournament->id, $userTournamentsIds), // Condizione di visibilità
                            ],
                            [
                                'text' => __('menu.standing'),
                                'url' => 'auth/standings/' . $tournament->id,
                                'icon' => 'fa fa-list-ol',
                            ],
                            [
                                'text' => __('menu.tournament_regulation'),
                                'url' => 'auth/tournament_regulation/' . $tournament->id,
                                'icon' => 'fa fa-gavel',
                            ],
                        ];

                        // Filtra il sottomenu per rimuovere le voci non visibili
                        $submenu = array_filter($submenu, function($item) use ($userTournamentsIds) {
                            return !isset($item['visible']) || $item['visible'];
                        });

                        $event->menu->add([
                            'text' => strtoupper($tournament->short_name),
                            'image' => $tournament->getLogo(),
                            'submenu' => $submenu
                        ]);
                    }
                }

                $event->menu->add([
                    'text' => __('menu.old_tournaments'),
                    'url' => 'auth/old_tournaments',
                    'icon' => 'fa fa-archive',
                ]);

                $event->menu->add([
                    'text' => __('menu.champions_list'),
                    'url' => 'auth/champions_list',
                    'icon' => 'fa fa-medal',
                ]);

                $event->menu->add([
                    'text' => __('menu.general_regulation'),
                    'url' => 'auth/general_regulation',
                    'icon' => 'fa fa-gavel',
                ]);

                $event->menu->add([
                    'text' => __('menu.contact_us'),
                    'url' => 'auth/contact_us',
                    'icon' => 'fa fa-envelope',
                ]);

                //@TODO: AGGIUNGERE SOLO ADMIN $event->menu->add('Admin');
                $event->menu->add([
                    'text' => 'Admin',
                    'icon_color' => 'red',
                    'can' => 'admin',
                    'submenu' => [
                        [
                            'text' => trans('menu.create_category'),
                            'url' => 'auth/admin/categories/add',
                            'icon_color' => 'red',
                            'can' => 'admin'
                        ],
                        [
                            'text' => __('menu.create_newsfeed'),
                            'url' => 'auth/admin/newsfeeds/add',
                            'icon_color' => 'red',
                            'can' => 'admin'
                        ],
                        [
                            'text' => trans('menu.create_event'),
                            'url' => '/auth/admin/events/add',
                            'icon_color' => 'red',
                            'can' => 'admin'
                        ],
                        [
                            'text' => trans('menu.available_events'),
                            'url' => '/auth/admin/events/get_available',
                            'icon_color' => 'red',
                            'can' => 'admin'
                        ],
                        [
                            'text' => __('menu.make_results'),
                            'url' => 'auth/admin/calculate',
                            'icon_color' => 'red',
                            'can' => 'admin'
                        ],
                        [
                            'text' => trans('menu.tournaments_manage'),
                            'url' => route('admin.tournaments.index'),
                            'icon_color' => 'red',
                            'can' => 'admin'
                        ],

                        [
                            'text' => trans('menu.phases_manage'),
                            'url' => route('admin.phases.index'),
                            'icon_color' => 'red',
                            'can' => 'admin'
                        ],
                        [
                            'text' => trans('menu.groups_manage'),
                            'url' => route('admin.groups.index'),
                            'icon_color' => 'red',
                            'can' => 'admin'
                        ],
                        [
                            'text' => __('menu.hall_of_fame'),
                            'url' => 'auth/admin/hall_of_fames',
                            'icon_color' => 'red',
                            'can' => 'admin'
                        ],
                    ]
                ]);
            });
    }
}
