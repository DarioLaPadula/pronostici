<?php namespace App;

use App\Notifications\MailResetPasswordNotification;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public $timestamps = true;

    /**
     * Hash password
     * @param $input
     */
    public function setPasswordAttribute($input)
    {
        if ($input)
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
    }

    public function isAdmin() {
        return $this->role_id == '2';
    }

    /**
     * Get the points record associated with the bet.
     */
    public function points()
    {
        return $this->hasMany('App\Point');
    }

    /**
     * Get the tournaments record associated with the result.
     */
    public function tournament()
    {
        return $this->belongsToMany('App\Tournament', 'tournaments_users');
    }

    /**
     * Define n-n relationship with \Groups
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function groups()
    {
        return $this->belongsToMany('App\Group')
            ->withPivot(['bonus']);
    }

    /**
     * Get the results record associated with the user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function results()
    {
        return $this->hasMany('App\Result');
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MailResetPasswordNotification($token));
    }

    /**
     * Return the path with the users avatars
     *
     * @return string
     */
    public function getProfileImage()
    {
        if ($this->avatar) {
            return config('custom.users_images_path') . $this->avatar;
        }
        return config('custom.users_images_path')  . config('custom.default_user_image');
    }

    /**
     * Return the path with the users avatars
     *
     * @return string
     */
    public function adminlte_image()
    {
        if ($this->avatar) {
            return config('custom.users_images_path') . $this->avatar;
        }
        return config('custom.users_images_path')  . config('custom.default_user_image');
    }
}
