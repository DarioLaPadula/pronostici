<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = 'messages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'tournament_id', 'message',
    ];

    /**
     * Get the message record associated with the bet.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
