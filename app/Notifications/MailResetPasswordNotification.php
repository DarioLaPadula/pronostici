<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\File;

class MailResetPasswordNotification extends Notification
{
    use Queueable;

    /**
     * The password reset token.
     *
     * @var string
     */
    public $token;

    /**
     * The callback that should be used to build the mail message.
     *
     * @var \Closure|null
     */
    public static $toMailCallback;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if (static::$toMailCallback) {
            return call_user_func(static::$toMailCallback, $notifiable, $this->token);
        }

        $text = [
            'Reset_Password' => __('email.Reset_Password'),
            'Reset_Password_notification' => __('email.Reset_Password_notification'),
            'Reset_Password_first_text' => __('email.Reset_Password_first_text'),
            'Reset_Password_button' => __('email.Reset_Password_button'),
            'Reset_Password_url' => __('email.Reset_Password_url'),
            'Reset_Password_ignore' => __('email.Reset_Password_ignore'),
        ];

        return (new MailMessage)
            ->subject(__('Reset Password notification'))
            ->from('bettingameinfo@gmail.com')
            ->markdown('email.password_recovery', [
                'link' =>  url(config('app.url').route('password.reset', $this->token, false)),
                'text' => $text,
                'logo' => asset('img/favicon.png'),

            ]);
    }

    /**
     * Set a callback that should be used when building the notification mail message.
     *
     * @param  \Closure  $callback
     * @return void
     */
    public static function toMailUsing($callback)
    {
        static::$toMailCallback = $callback;
    }
}
