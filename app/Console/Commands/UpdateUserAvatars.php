<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;
use Laravolt\Avatar\Avatar;
use Laravolt\Avatar\Facade;
use function PHPUnit\Framework\stringStartsWith;

class UpdateUserAvatars extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update-user-avatars';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->info('Updating profile images...');

        User::each(function (User $user) {
            if (str_starts_with($user->avatar, 'default')) {
                $avatar = 'default' . $user->id . '.png';
                Facade::create($user->name)->save(base_path() . '/public/storage/uploads/avatars/' . $avatar);
                $user->avatar = $avatar;
                $user->save();
            }
        });

        $this->info('Profile image update process completed.');

        return 0;
    }
}
