<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SurveyResult extends Model
{
     protected $table = 'surveys_results';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'choice_id'
    ];

    /**
     * Get the survey_choice record associated with the result.
     */
    public function survey_choice()
    {
        return $this->belongsTo('App\SurveyChoice');
    }

}
