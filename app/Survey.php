<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    protected $table = 'surveys';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'message'
    ];

    /**
     * Get the survey_choice record associated with the result.
     */
    public function survey_choice()
    {
        return $this->hasMany('App\SurveyChoice');
    }
}
