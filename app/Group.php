<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
     protected $table = 'groups';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'phase_id', 'group',
    ];

    /**
     * Define n-n relationship with \Users
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\User')
            ->withPivot(['bonus'])
            ->withTimestamps();
    }

    /**
     * Get the tournament record associated with the group.
     */
    public function tournament()
    {
        return $this->belongsTo('App\Tournament');
    }

    /**
     * Get the phase record associated with the group.
     */
    public function phase()
    {
        return $this->belongsTo('App\Phase');
    }


}
