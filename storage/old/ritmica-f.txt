Ginnastica ritmica - All-Around individuale - Vincitore Finale
2024-07-26 23:59:00
Aghamirova, Zohra
51,00
Atamanov, Daria
3,50
Baldassarri, Milena
12,00
Bautista, Alba
6,50
Berezina, Polina
51,00
Domingos, Barbara
101,00
Dragan, Annaliese
23,00
Griskenas, Evita
51,00
Ikromova, Takhmina
19,00
Kaleyn, Boryana
10,00
Karbanov, Helene
41,00
Kiroi-bogatyreva, Alexandra
101,00
Kolosov, Margarita
16,00
Nikolova, Stiliana
9,00
Onofriichuk, Taisiia
19,00
Philaphandeth, Praewa Misato
201,00
Pigniczki, Fanni
41,00
Raffaeli, Sofia
5,00
Saleh, Aliaa
201,00
Taniyeva, Elzhana
41,00
Tugolukova, Vera
41,00
Varfolomeev, Darja
2,75
Vedeneeva, Ekaterina
12,00
Wang, Zilu
23,00
