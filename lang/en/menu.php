<?php

return [

    'add_results' => 'Add results',
    'available_events' => 'Available events',
    'bar' => 'Bar',
    'create_category' => 'Create category',
    'create_event' => 'Create event',
    'create_tournament' => 'Create tournament',
    'dashboard' => 'Dashboard',
    'list_results' => 'Results list',
    'make_results' => 'Compute results',
    'my_results' => 'My results',
    'News' => 'News',
    'standing' => 'Standings',
    'tournaments_available' => 'Tournaments available',

];

?>
