<?php
return [
    'Reset_Password' => 'Reset Password',
    'Reset_Password_notification' => 'Recupero password ' . env('APP_NAME'),
    'Reset_Password_first_text' => 'Stai ricevendo questa mail perchè hai richiesto il reset password sul sito ' . env('APP_NAME') . '.',
    'Reset_Password_button' => 'Imposta nuova password',
    'Reset_Password_url' => 'Se hai problemi con il bottone di recupero password, copia questo url sul tuo browser: ',
    'Reset_Password_ignore' => 'Se non hai fatto tu la richiesta, allora ignora questa email.',
];
