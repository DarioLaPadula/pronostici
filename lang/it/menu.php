<?php

return [

    'add_results' => 'Inserisci pronostici',
    'available_events' => 'Eventi disponibili',
    'bar' => 'Il bar',
    'create_category' => 'Crea categoria',
    'create_event' => 'Crea Evento',
    'create_newsfeed' => 'Crea News',
    'create_tournament' => 'Crea Torneo',
    'create_phase' => 'Crea Phase',
    'create_group' => 'Crea Gruppo',
    'dashboard' => 'Dashboard',
    'edit_tournment' => 'Modifica Torneo',
    'list_results' => 'Elenco pronostici',
    'make_results' => 'Calcola risulati',
    'my_results' => 'I miei pronostici',
    'news' => 'News',
    'standing' => 'Classifica',
    'tournaments_available' => 'Tornei disponibili',
    'tournaments_manage' => 'Gestione tornei',
    'phases_manage' => 'Gestione fasi',
    'groups_manage' => 'Gestione gruppi',
    'tournaments_list' => 'Lista tornei',
    'phases_list' => 'Lista fasi',
    'groups_list' => 'Lista gruppi',
    'old_tournaments' => 'Tornei passati',
    'champions_list' => 'Albo d\'oro',
    'hall_of_fame' => 'Albo d\'oro',
    'general_regulation' => 'Regolamento generale',
    'tournament_regulation' => 'Regolamento torneo',
    'contact_us' => 'Contattaci',
];

?>
