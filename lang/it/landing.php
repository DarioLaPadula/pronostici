<?php
// landing.php

return [
    'contact' => [
        'section_title' => 'Contattaci',
        'name_placeholder' => 'Il tuo Nome',
        'name_error' => 'Per favore inserisci il tuo nome',
        'email_placeholder' => 'La tua Email',
        'email_error' => 'Per favore inserisci una email valida',
        'subject_placeholder' => 'Oggetto',
        'message_placeholder' => 'Il tuo Messaggio',
        'message_error' => 'Scrivi il tuo messaggio',
        'submit_button' => 'Invia Messaggio',
        'success' => 'Grazie per averci contattato. Ti risponderemo il prima possibile.',
        'error' => 'Si è verificato un problema durante l\'invio. Riprova più tardi.',
    ]
];
