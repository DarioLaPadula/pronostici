<?php

namespace Tests\Feature\Http\Controllers;

use App\Http\Controllers\PagesController;
use Tests\TestCase;

/**
 * Class PagesControllerTest.
 *
 * @covers \App\Http\Controllers\PagesController
 */
class PagesControllerTest extends TestCase
{
    /**
     * @var PagesController
     */
    protected $pagesController;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->pagesController = new PagesController();
        $this->app->instance(PagesController::class, $this->pagesController);
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown(): void
    {
        parent::tearDown();

        unset($this->pagesController);
    }

    public function testIndex(): void
    {
        /** @todo This test is incomplete. */
         $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    public function testDashboard(): void
    {
        /** @todo This test is incomplete. */
         $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    public function testResults(): void
    {
        /** @todo This test is incomplete. */
         $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    public function testEventAjax(): void
    {
        /** @todo This test is incomplete. */
         $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    public function testEvent(): void
    {
        /** @todo This test is incomplete. */
         $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    public function testShow(): void
    {
        /** @todo This test is incomplete. */
         $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    public function testAdd(): void
    {
        /** @todo This test is incomplete. */
         $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    public function testStandings(): void
    {
        /** @todo This test is incomplete. */
         $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    public function testStore(): void
    {
        /** @todo This test is incomplete. */
         $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    public function testMessages(): void
    {
        /** @todo This test is incomplete. */
         $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    public function testStore_message(): void
    {
        /** @todo This test is incomplete. */
         $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    public function testTournaments_available(): void
    {
        /** @todo This test is incomplete. */
         $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    public function testOld_tournaments(): void
    {
        /** @todo This test is incomplete. */
         $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    public function testChampions_list(): void
    {
        /** @todo This test is incomplete. */
         $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }
}
