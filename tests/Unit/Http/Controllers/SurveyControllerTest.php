<?php

namespace Tests\Feature\Http\Controllers;

use App\Http\Controllers\SurveyController;
use Tests\TestCase;

/**
 * Class SurveyControllerTest.
 *
 * @covers \App\Http\Controllers\SurveyController
 */
class SurveyControllerTest extends TestCase
{
    /**
     * @var SurveyController
     */
    protected $surveyController;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->surveyController = new SurveyController();
        $this->app->instance(SurveyController::class, $this->surveyController);
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown(): void
    {
        parent::tearDown();

        unset($this->surveyController);
    }

    public function testSurvey(): void
    {
        /** @todo This test is incomplete. */
         $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }
}
