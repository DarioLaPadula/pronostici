<?php

namespace Tests\Feature\Http\Controllers\AjaxController;

use App\Http\Controllers\AjaxController\AjaxPagesController;
use Tests\TestCase;

/**
 * Class AjaxPagesControllerTest.
 *
 * @covers \App\Http\Controllers\AjaxController\AjaxPagesController
 */
class AjaxPagesControllerTest extends TestCase
{
    /**
     * @var AjaxPagesController
     */
    protected $ajaxPagesController;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->ajaxPagesController = new AjaxPagesController();
        $this->app->instance(AjaxPagesController::class, $this->ajaxPagesController);
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown(): void
    {
        parent::tearDown();

        unset($this->ajaxPagesController);
    }

    public function testStore_tournament_user(): void
    {
        /** @todo This test is incomplete. */
         $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }
}
