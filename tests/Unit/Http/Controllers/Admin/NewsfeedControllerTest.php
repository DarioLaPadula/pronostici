<?php

namespace Tests\Feature\Http\Controllers\Admin;

use App\Http\Controllers\Admin\NewsfeedController;
use Tests\TestCase;

/**
 * Class NewsfeedControllerTest.
 *
 * @covers \App\Http\Controllers\Admin\NewsfeedController
 */
class NewsfeedControllerTest extends TestCase
{
    /**
     * @var NewsfeedController
     */
    protected $newsfeedController;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->newsfeedController = new NewsfeedController();
        $this->app->instance(NewsfeedController::class, $this->newsfeedController);
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown(): void
    {
        parent::tearDown();

        unset($this->newsfeedController);
    }

    public function testIndex(): void
    {
        /** @todo This test is incomplete. */
         $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    public function testCreate(): void
    {
        /** @todo This test is incomplete. */
         $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    public function testStore(): void
    {
        /** @todo This test is incomplete. */
         $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    public function testEdit(): void
    {
        /** @todo This test is incomplete. */
         $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    public function testUpdate(): void
    {
        /** @todo This test is incomplete. */
         $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    public function testDestroy(): void
    {
        /** @todo This test is incomplete. */
         $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }
}
