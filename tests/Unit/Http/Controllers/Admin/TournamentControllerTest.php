<?php

namespace Tests\Feature\Http\Controllers\Admin;

use App\Http\Controllers\Admin\TournamentController;
use Tests\TestCase;

/**
 * Class TournamentControllerTest.
 *
 * @covers \App\Http\Controllers\Admin\TournamentController
 */
class TournamentControllerTest extends TestCase
{
    /**
     * @var TournamentController
     */
    protected $tournamentController;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->tournamentController = new TournamentController();
        $this->app->instance(TournamentController::class, $this->tournamentController);
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown(): void
    {
        parent::tearDown();

        unset($this->tournamentController);
    }

    public function testList_tournament(): void
    {
        /** @todo This test is incomplete. */
         $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    public function testAdd_tournament(): void
    {
        /** @todo This test is incomplete. */
         $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    public function testStore_tournament(): void
    {
        /** @todo This test is incomplete. */
         $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    public function testEdit_tournament(): void
    {
        /** @todo This test is incomplete. */
         $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    public function testUpdate_tournament(): void
    {
        /** @todo This test is incomplete. */
         $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }
}
