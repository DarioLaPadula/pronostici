<?php

namespace Tests\Feature\Http\Controllers\User;

use App\Http\Controllers\User\UserController;
use Tests\TestCase;

/**
 * Class UserControllerTest.
 *
 * @covers \App\Http\Controllers\User\UserController
 */
class UserControllerTest extends TestCase
{
    /**
     * @var UserController
     */
    protected $userController;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->userController = new UserController();
        $this->app->instance(UserController::class, $this->userController);
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown(): void
    {
        parent::tearDown();

        unset($this->userController);
    }

    public function testEdit(): void
    {
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }

    public function testUpdate(): void
    {
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }
}
