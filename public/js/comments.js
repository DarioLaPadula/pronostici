function store ($button) {
    var $input = $button.closest('.input-group').find('input').val();

    if ($input) {
        $.ajax({
            method: "POST",
            url: config.routes[0].comment_store,
            data: {
                'newsfeed_id': $button.attr('newsfeed_id'),
                'comment': $button.closest('.input-group').find('input').val(),
                '_token': config.token,
            }
        })
            .done(function (msg) {
                if (msg.status) {
                    location.reload(true);
                }
                else {
                    // @TODO Sostituire con modale
                    alert(msg.message);
                }
            });
    }
}


$(document).ready(function () {
    var $button = $('button.send-comment');
    var $delete_comment = $('.delete-comment');

    $button.on('click', function () {
        store($(this));
    });

    $button.closest('.input-group').find('input').on('keydown', function (e) {
        if (e.keyCode === 13) {
            store($(this).closest('.input-group').find('button'));
        }
    });

    $delete_comment.on('click', function (e) {
        $.ajax({
            method: "POST",
            url: config.routes[0].comment_delete,
            data: {
                'comment_id': $(e.target).attr('c_id'),
                '_token': config.token,
            }
        })
            .done(function (msg) {
                if (msg.status) {
                    $(e.target).closest('.box-comment').first().remove();
                }
                else {
                    // @TODO Sostituire con modale
                    alert(msg.message);
                }
            });
    });
});