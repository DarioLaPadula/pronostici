<?php

use Illuminate\Database\Seeder;

class PointSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $results = \App\Result::select('results.user_id', DB::raw('SUM(bets.points) AS sum'))
            ->join('events', 'results.event_id', 'events.id')
            ->join('bets', 'results.bet_id', 'bets.id')
            ->where('events.category_id', 24)
            ->where('results.win', 1)
            ->groupBy('results.user_id')
            ->orderBy('sum', 'DESC')
            ->get();

        foreach($results as $result) {
            $points = \App\Point::where('user_id', $result->user_id)
                ->where('tournament_id', 24)
                ->first();
            // dump($points->points);
            if ($result->user_id == 1) {
                dump($points->user_id);
                dump($points->points);
            }
            $points->fill([
                'points' => $result->sum
            ]);
            if ($result->user_id == 1) {
                dump($points->user_id);
                dump($points->points);
            }



            $points->save();
        }
    }
}
