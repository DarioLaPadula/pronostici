<?php

use Illuminate\Database\Seeder;

class PhaseTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('phase_types')->delete();

        \DB::table('phase_types')->insert([
                [
                    'id' => 1,
                    'name' => 'group',
                    'created_at' => '2019-03-13 15:39:00',
                    'updated_at' => '2019-03-13 15:39:00',
                ],
                [
                    'id' => 2,
                    'name' => 'knockout',
                    'created_at' => '2019-03-13 15:39:00',
                    'updated_at' => '2019-03-13 15:39:00',
                ]
        ]);
    }
}
