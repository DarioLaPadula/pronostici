<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableGroupsUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // group_user
        Schema::create('group_user', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('group_id')->references('id')->on('groups');
            $table->unsignedInteger('user_id')->references('id')->on('users');

            $table->timestamps();
        });

        $groups = \App\Group::all();

        DB::table('groups')->truncate();

        $names = [];
        $currentId = -1;
        foreach ($groups as $group) {

            if (!in_array($group->group, $names)) {
                $names[] = $group->group;
                //$currentId = $group->id;
                DB::table('groups')->insert(
                    [
                        'id' => sizeof($names),
                        'phase_id' => $group->phase_id,
                        'bonus' => $group->bonus,
                        'group' => $group->group,
                        'created_at' => $group->created_at,
                        'updated_at' => $group->updated_at,
                    ]
                );
            }

            DB::table('group_user')->insert(
                [
                    'group_id' => sizeof($names),
                    'user_id' => $group->user_id,
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
                ]
            );

        }

        Schema::table('groups', function (Blueprint $table) {
            $table->dropColumn('user_id');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_user');
    }
}

/*
 * alter table `phases` add `start_date` datetime not null after `n_qualified`, add `end_date` datetime not null after `start_date`
create table `group_user` (`id` int unsigned not null auto_increment primary key, `group_id` int unsigned not null, `user_id` int unsigned not null, `created_at` timestamp null, `updated_at` timestamp null) default character set utf8mb4 collate 'utf8mb4_unicode_ci'
select * from `groups`
alter table `groups` drop `user_id`

 */