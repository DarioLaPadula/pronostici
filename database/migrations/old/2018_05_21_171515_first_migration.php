<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FirstMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // users
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 32);
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

        // events
        Schema::create('tournaments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 32);
            $table->timestamps();
        });

        // categories
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 128);
            $table->timestamps();
        });

        // events
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 128);
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('categories');
            $table->timestamps();
        });

        // bets
        Schema::create('bets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 128);
            $table->float('points');
            $table->integer('event_id')->unsigned();
            $table->foreign('event_id')->references('id')->on('events');
            $table->timestamps();
        });

        // results
        Schema::create('results', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bet_id')->unsigned();
            $table->foreign('bet_id')->references('id')->on('bets');
            $table->integer('event_id')->unsigned();
            $table->foreign('event_id')->references('id')->on('bets');
            $table->timestamps();
        });

        // points
        Schema::create('points', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tournament_id')->unsigned();
            $table->foreign('tournament_id')->references('id')->on('tournaments');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('bets');
            $table->float('points');
            $table->dateTime('last_update');
            $table->timestamps();
        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('tournaments');
        Schema::dropIfExists('categories');
        Schema::dropIfExists('events');
        Schema::dropIfExists('bets');
        Schema::dropIfExists('results');
        Schema::dropIfExists('points');
    }
}
