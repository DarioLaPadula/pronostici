<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserIdToResults extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('results', function (Blueprint $table) {
             $table->integer('user_id')->unsigned()->after('event_id');
             //$table->foreign('event_id')->references('id')->on('bets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::table('events', function (Blueprint $table) {
             $table->dropColumn(['user_id']);
        });
    }
}
