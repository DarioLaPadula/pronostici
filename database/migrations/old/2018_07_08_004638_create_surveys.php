<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         // surveys
        Schema::create('surveys', function (Blueprint $table) {
            $table->increments('id');
            $table->text('message');
            $table->timestamps();
        });

        // surveys_choices
        Schema::create('surveys_choices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('survey_id')->unsigned();
            $table->text('response');
            $table->timestamps();
        });

        // surveys_results
        Schema::create('surveys_results', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('choice_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surveys');
        Schema::dropIfExists('surveys_choices');
        Schema::dropIfExists('surveys_results');
        
    }
}
