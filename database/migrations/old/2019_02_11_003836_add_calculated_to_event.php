<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCalculatedToEvent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->text('final_summary')->nullable()->after('deadline');
            $table->boolean('calculated')->default(false)->after('deadline');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->dropColumn('final_summary');
            $table->dropColumn('calculated');
        });
    }
}
// alter table `events` add `final_summary` text null after `deadline`, add `calculated` tinyint(1) not null default '0' after `deadline`
// update events set calculated = 1
