<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTournamentsUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tournaments_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tournament_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->timestamps();
        });

        // alter table `tournaments_users` add `tournament_id` int unsigned not null, add `user_id` int unsigned not null, add `created_at` timestamp null, add `updated_at` timestamp null
        // select count(*) as aggregate from `users`

        for($i=1; $i <= App\User::count(); $i++) {
            Illuminate\Support\Facades\DB::table('tournaments_users')->insert(
            [
                'tournament_id' => 1,
                'user_id' => $i
            ]
        );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tournaments_users');
    }
}
