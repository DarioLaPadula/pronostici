<?php
use Illuminate\Support\Carbon ?>

{{-- resources/views/admin/dashboard.blade.php --}}

@extends('page')

@section('css')
    @parent
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('content_header')
    @parent
    <h1>{{ config('app.name') }} - Lista tornei</h1>
@endsection
@section('content')

    <div class="card">
        <div class="card-body">
            @if($tournaments->isEmpty())
                Al momento non è possibile iscriversi a nessun nuovo torneo
            @else
                <table class="table datatable table-bordered responsive">
                    <tbody>
                    @foreach($tournaments as $tournament)
                        <tr>
                            <td class="align-middle"><strong>{{$tournament->name}}</strong></td>
                            <td class="align-middle">
                                {{ __('custom.deadline_subscription') }}:
                                {{ \Illuminate\Support\Carbon::parse($tournament->deadline_subscription)->format('d/m/Y H:i') }}<br>
                                Tempo rimanente: <span class="deadline"
                                                                   date="{{$tournament->deadline_subscription}}"></span>
                            </td>
                            <td class="align-middle text-center"> @if (!$tournament->tournament_user_id)
                                    <button class="btn btn-success subscribe" value="{{$tournament->id}}"
                                            type="button">{{trans('custom.subscribe')}}
                                    </button>
                                @else
                                    <div class="text-success text-18 bold">
                                        <span class="text-18 bold btn btn-sm btn-success">
                                            <i class="fa fa-check-circle"></i>
                                        </span>
                                      </div>
                                @endif</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>



            @endif
        </div>
    </div>
@stop

@section('js')

    <script src="{{ asset('js/plugin/jquery.countdown.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            var $button = $('button.subscribe');
            $button.click(function () {
                var $clicked = $(this);
                $.ajax({
                    method: "POST",
                    url: '{{ route('pages.add_tournament_user') }}',
                    data: {
                        'tournament_id': $clicked.val(),
                        '_token': "{{ csrf_token() }}",
                    }
                })
                    .done(function (msg) {
                        if (msg.status) {
                            $clicked.hide(200);
                            $clicked.closest('td').append('<div class="text-success text-18 bold">'+
                                '<span class="text-18 bold btn btn-sm btn-success">' +
                                '<i class="fa fa-check-circle"></i>'+
                                '</span></div>');
                            setTimeout(function () {
                                //window.location.replace('{{ url('auth/standings/')  }}' + '/' + $clicked.val());
                            }, 1000);
                        } else {
                            // @TODO Sostituire con modale
                            alert(msg.message);
                        }
                    });
            });
            $(".deadline").each(function (index) {
                $(this).countdown($(this).attr('date'), function (event) {
                    $(this).text(event.strftime('%D giorni %Hh:%Mm:%Ss'));
                });
            });
        });

    </script>

@endsection

