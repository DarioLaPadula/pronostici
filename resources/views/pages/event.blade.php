<?php
use Illuminate\Support\Carbon ?>

{{-- resources/views/admin/dashboard.blade.php --}}

@extends('page')

@section('title', 'Pronostici ' .  $event->name)

@section('content')
@parent
<h1>{{ config('app.name') }} - {{ $event->name }}</h1>


<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title"><strong>{{ $event->name }}</strong> ({{ Carbon::parse($event->deadline)->format('d/m/Y H:i') }})</h3>
                @if ($tournamentId)
                <span class="pull-right">
                    <a href="{{ route('pages.show', $tournamentId) }}" type="button" class="btn btn-primary btn-lg">Torna indietro</a>
                </span>
                @endif
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table">
                    <tr>
                        <th width="30%"></th>
                        <th></th>
                        <th></th>
                    </tr>

                    @foreach ($results as $result)
                    <tr>
                       @if(Carbon::parse($event->deadline)->isFuture() && $event->private)
                            <td>{{ $result->username }}</td>
                            <td>
                                {{ strtoupper(__('custom.private_bet')) }}
                            </td>
                        @else

                            <td>{{ $result->username }}</td>
                            <td>
                                @if ($result->win <= 0)
                                {{ $result->bet }}
                                @else
                                <span class="bold">{{ $result->bet }}</span>
                                @endif
                            </td>
                            <td>@if ($result->win <= 0)
                                {{ $result->points }}
                                @else
                                <span class="text-success bold text-14">{{ $result->points }}</span>
                                @endif
                        @endif
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</div>

@stop
