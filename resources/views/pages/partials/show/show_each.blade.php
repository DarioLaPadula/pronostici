<?php

use Illuminate\Support\Carbon ?>

<div class="card page-show">
    <div class="p-2 text-primary border-bottom">
        <strong>{{$text}}</strong>
    </div>
    <div class="p-2">
        @if ($mappedEvents->isEmpty())
            <div class="row">
                <div class="col-12 no-match">
                    <strong>
                        <span>Nessun match</span>
                    </strong>
                </div>
            </div>
        @else
            <div class="row">
                @foreach($mappedEvents as $deadline => $events)
                    <div class="col-12">
                        @php
                            if (Carbon::parse($deadline)->isToday()) {
                               $stringDeadline = Carbon::parse($deadline)->format('H:i');
                            }

                            else {
                                $stringDeadline = Carbon::parse($deadline)->format('d/m/Y H:i');
                            }
                        @endphp

                        <div class="row mb-4">
                            <div class="col-12">
                                <div class="text-center bold @if(Carbon::parse($deadline)->lt(Carbon::parse())) text-danger @else text-success @endif">
                                    {{ $stringDeadline }}
                                </div>
                            </div>
                        </div>

                        <table class="datatable responsive mb-5 table-layout-fixed stripe">
                            <thead class="d-none">
                                <th data-priority="1">
                                    &nbsp;
                                </th>
                                <th data-priority="1">
                                    &nbsp;
                                </th>
                                <th data-priority="1">
                                    &nbsp;
                                </th>
                            </thead>
                            <tbody>
                            @foreach ($events as $key => $event)
                                <tr>
                                    <td class="pl-0 nowrap">
                                        {{$event->name}}
                                    </td>
                                    <td>
                                        <span class="text-info">{{ $event->final_summary ?? ' ' }}</span>
                                    </td>
                                    <td class="text-right">
                                        <a class="event pointer" data-event_id="{{ $event->id }}"
                                           data-tournament_id="{{ $tournamentId }}">
                                            <button class="btn btn-primary btn-xs"><i class="fas fa-eye"></i></button>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            {{--Per non visualizzare border aggiungo tfoot "fittizio" --}}
                            <tfoot class="d-none">
                                <th data-priority="1">
                                    &nbsp;
                                </th>
                                <th data-priority="1">
                                    &nbsp;
                                </th>
                                <th data-priority="1">
                                    &nbsp;
                                </th>
                            </tfoot>
                        </table>
                    </div>
                @endforeach

            </div>
        @endif
    </div>
</div>

@once
    @include('extra_large_modal')
    @include('loader')
    @include('partials.datatable', ['paging' => 'false'])
    @include('partials.script.event_ajax_js')
@endonce
