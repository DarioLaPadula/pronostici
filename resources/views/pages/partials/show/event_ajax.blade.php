<?php

use Illuminate\Support\Carbon ?>

<div id="show_results" data-event="{{ $event->name }}">
    @if ($results->isEmpty())
        {{ __('custom.show_results_empty') }}
    @else
    <table class="datatable table-layout-fixed stripe">
            <thead class="d-none">
                <th>
                    &nbsp;
                </th>
                <th>
                    &nbsp;
                </th>
                <th>
                    &nbsp;
                </th>
            </thead>
            <tbody>
            @foreach ($results as $result)
                <tr class="{{ auth()->user()->name == $result->username ? 'bold' : '' }}">
                    @if(Carbon::parse($event->deadline)->isFuture() && $event->private)
                        <td class="pl-0" style="width:50%">
                            <div class="inline-block">
                            <img class="img-circle img-sm avatar"
                                 src="{{ config('custom.users_images_path') . $result->avatar }}">
                                <span>{{ $result->username }}</span>
                            </div>
                        </td>
                        <td style="width:35%">
                            {{ __('custom.private_vote') }}
                        </td>
                        <td style="display: none; width: 15%;"></td>
                    @else
                        <td class="pl-0" style="width:50%">
                            <div class="inline-block">
                            <img class="img-circle img-sm avatar"
                                 src="{{ config('custom.users_images_path') . $result->avatar }}">
                            @if ($result->win == 0)
                                <span>{{ $result->username }}</span>
                            @elseif ($result->win < 0)
                                <del class="text-danger text-14">{{ $result->username }}</del>
                            @else
                                <span class="text-success bold text-14">{{ $result->username }}</span>
                            @endif
                            </div>
                        </td>
                        <td style="width:35%">
                            @if ($result->win == 0)
                                {{ $result->bet }}
                            @elseif ($result->win < 0)
                                <del class="text-danger text-14">{{ $result->bet }}</del>
                            @else
                                <span class="text-success bold text-14">{{ $result->bet }}</span>
                            @endif
                        </td>
                        <td style="width:15%">
                            @if ($result->win == 0)
                                {{ number_format($result->points, 2) }}
                            @elseif ($result->win < 0)
                                <del class="text-danger text-14">{{ number_format($result->points, 2) }}</del>
                            @else
                                <span class="text-success bold text-14">{{ number_format($result->points, 2) }}</span>
                            @endif
                        </td>
                    @endif
                </tr>
            @endforeach
            </tbody>
            <tfoot class="d-none">
                <th>
                    &nbsp;
                </th>
                <th>
                    &nbsp;
                </th>
                <th>
                    &nbsp;
                </th>
            </tfoot>
    </table>
    @endif
</div>

<script>
    doDatatable();
</script>
