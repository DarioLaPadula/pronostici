@extends('page')

@section('content_header')
    <h1>{{ __('custom.general_regulation') }}</h1>
@stop

@section('content')
    @parent

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="p-2">
                    @include('partials.general_regulation')
                </div>
            </div>
        </div>
    </div>

@stop

