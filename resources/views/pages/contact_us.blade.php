@extends('page')

@section('content_header')
    <h1>{{ __('custom.contact_us') }}</h1>
@stop

@section('content')
    @parent

                    @include('partials.messages')

                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <h3 class="card-title">{{ __('custom.send_message_to_staff') }}</h3>
                        </div>
                        <!-- /.card-header -->
                        <form action="{{ route('pages.send_contact_mail') }}" method="POST">
                        @csrf <!-- Token CSRF per sicurezza -->
                            <div class="card-body">
                                <div class="form-group">
                                    <textarea id="compose-textarea" name="message"
                                              class="form-control @error('message') is-invalid @enderror"
                                              style="height: 300px;" placeholder="{{ __('custom.write_message_here') }}">{{ old('message') }}</textarea>
                                    @error('message')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <div class="float-right">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="far fa-envelope"></i> {{ __('custom.send') }}
                                    </button>
                                </div>
                            </div>
                            <!-- /.card-footer -->
                        </form>
                    </div>
@stop
