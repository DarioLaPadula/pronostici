<?php

use Illuminate\Support\Carbon ?>

@extends('page')

@section('css')
    @parent
@stop

@section('content_header')
    <h1>{{ config('app.name') }} - Inserisci Pronostico</h1>
@stop

@section('content')
    @parent

    <form action="{{ url('auth/add/' . $tournamentId) }}" method="post">
        {!! csrf_field() !!}

        <div class="card">
            <div class="card-body">

                @php
                    $i=0;
                @endphp
                <div class="row">
                    @foreach ($mappedEvents as $deadline => $events)
                        <div class="col-12 mb-4">
                            <div class="bold text-primary">{{ $deadline }}</div>

                            <div class="row">
                            @foreach($events as $event)
                                @php
                                    //@TODO: MIGLIORARE - VEDERE COME SI PUÒ PRENDERE DALLA QUERY
                                    $result = collect($event->result)->filter(function ($item, $key) {
                                        return $item->user_id == Auth::user()->id;
                                    })->first();
                                @endphp
                                <div class="mb-4 col-md-6 col-sm-6 col-12">
                                    <div class="bold">{{ $event->name }}</div>
                                    <x-adminlte-select2 name="results[{{$event->id}}]">
                                        <option disabled value {{ isset($result->id) ? '' : 'selected' }}> -- Seleziona --
                                        </option>
                                        @foreach ($event->bet as $bet)
                                            @php($i++)
                                            {{ $selected = (isset($result->id) && ($result->bet_id == $bet->id)) ? "selected" : "" }}
                                            <option {{ $selected }} value="{{ $bet->id }}">{{ $bet->name }}
                                                ({{  number_format($bet->points, 2, '.', ',')  }})
                                            </option>
                                        @endforeach
                                    </x-adminlte-select2>
                                </div>
                            @endforeach
                            </div>

                        </div>
                    @endforeach
                </div>
                @if ($i==0)
                    <p>Nessun match disponibile al momento</p>
                @else

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3 offset-md-9 col-xs-12 pull-right m-t">
                                <button type="submit"
                                        class="form-control btn btn-primary btn-block btn-flat">{{ trans('custom.save') }}</button>
                            </div>
                        </div>
                    </div>

                @endif

            </div>
        </div>
    </form>

@stop

@section('plugins.Select2', true)
@section('js')
    <script>
        //Initialize Select2 Elements
        $('select').select2({
            theme: 'bootstrap4'
        })

    </script>
@endsection

