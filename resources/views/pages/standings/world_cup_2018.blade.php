{{-- resources/views/admin/dashboard.blade.php --}}

@extends('page')

@section('title', 'Pronostici mondiali')

@section('content')
@parent

<h1>The Betting Game - Classifiche Mondiali 2018</h1>

<div class="card">
    <div class="card-body">
            <div class="col-xs-12 col-md-8 col-lg-9">
                                <div>
                    <h3 class="standing">Finale</h3>

                    <div>
                        <table class="table table-striped">
                            <thead>
                            <th colspan="3"><div class="bold text-18 text-center"></div></th>
                            </thead>
                            <tbody class="text-18 bold">
                                <tr>
                                    <td class="team text-right text-success text-22" style="width:45%"><span id="O1"> Gozer il Gozeriano 2.85</span></td>
                                    <td class="team text-center" style="width:10%"> - </td>
                                    <td class="team" style="width:45%"><span id="O2">Mirko P. 0.00</span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="text-22"><strong>Gozer il Gozeriano</strong> vince il torneo</div>
                </div>

                <div>
                    <h3 class="standing">Finale 3/4 posto (a grande richiesta delle interessate)</h3>
                    <div>
                        <table class="table table-striped">
                            <thead>
                            <th colspan="3"><div class="bold text-18 text-center"></div></th>
                            </thead>
                            <tbody class="text-18 bold">
                                <tr>
                                    <td class="team text-right" style="width:45%"><span id="O2">Manolo 0.00</span></td>
                                    <td class="team text-center" style="width:10%"> - </td>
                                    <td class="team text-success" style="width:45%"><span id="O1"> Ludovica Ciufoli 4.60</span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div>

                <div>
                    <h3 class="standing">Semifinali</h3>
                    <div>
                        <table class="table table-striped">
                            <thead>
                            <th colspan="3"><div class="bold text-18 text-center"></div></th>
                            </thead>
                            <tbody class="text-18 bold">
                                <tr>
                                    <td class="team text-right" style="width:45%"><span id="O1" class="text-success">Gozer il Gozeriano 23.50</span></td>
                                    <td class="team text-center" style="width:10%"> - </td>
                                    <td class="team" style="width:45%"><span id="O2">Manolo  8.75</span></td>
                                </tr>

                                <tr>
                                    <td class="team text-right"><span id="O5">Ludovica Ciufoli  1.68</span></td>
                                    <td class="team text-center"> - </td>
                                    <td class="team"><span id="O6" class="text-success">Mirko P. 2.07</span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>


                <div>
                    <h3 class="standing">Quarti</h3>
                    <div>
                        <table class="table table-striped">
                            <thead>
                            <th colspan="3"><div class="bold text-18 text-center"></div></th>
                            </thead>
                            <tbody class="text-18 bold">
                                <tr>
                                    <td class="team text-right text-success" style="width:45%"><span id="O1">Gozer il Gozeriano 7.02</span></td>
                                    <td class="team text-center" style="width:10%"> - </td>
                                    <td class="team" style="width:45%"><span id="O2">Massimo Palozzi 3.67</span></td>
                                </tr>

                                <tr>
                                    <td class="team text-right text-success"><span id="O3">Manolo 5.01</span></td>
                                    <td class="team text-center"> - </td>
                                    <td class="team"><span id="O4">Salvatore lavorgna  1.78</span></td>
                                </tr>

                                <tr>
                                    <td class="team text-right"><span id="O5">Alessandro zaffini 1.95</span></td>
                                    <td class="team text-center"> - </td>
                                    <td class="team text-success"><span id="O6">Ludovica Ciufoli 4.71</span></td>
                                </tr>

                                <tr>
                                    <td class="team text-right text-success"><span id="OA4">Mirko P. 3.26</span></td>
                                    <td class="team text-center"> - </td>
                                    <td class="team"><span id="OB1">Andrea Frizziero 2.95</span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <h3 class="standing">Ottavi</h3>
                                <div>
                    <table class="table table-striped">
                        <thead>
                            <tr><th colspan="3"><div class="bold text-18 text-center"></div></th>
                        </tr></thead>
                        <tbody class="text-18 bold">
                            <tr>
                                <td class="team text-right text-success" style="width:45%"><span id="OA1">Gozer il Gozeriano 63.45</span></td>
                                <td class="team text-center" style="width:10%"> - </td>
                                <td class="team" style="width:45%"><span id="OB4">Fabio Gradasso 55.82</span></td>
                            </tr>

                            <tr>
                                <td class="team text-right"><span id="OC4">Davide S. 50.27</span></td>
                                <td class="team text-center"> - </td>
                                <td class="team text-success"><span id="OD1">Massimo Palozzi 61.54</span></td>
                            </tr>

                            <tr>
                                <td class="team text-right text-success"><span id="OA2">Manolo 55.95</span></td>
                                <td class="team text-center"> - </td>
                                <td class="team"><span id="OB3">Enrico 53.71</span></td>
                            </tr>

                            <tr>
                                <td class="team text-right text-success"><span id="OC3">Salvatore lavorgna 59.36</span></td>
                                <td class="team text-center"> - </td>
                                <td class="team"><span id="OD2">Fabrizio Di Coste 54.10</span></td>
                            </tr>

                            <tr>
                                <td class="team text-right text-success"><span id="OA3">Alessandro zaffini 56.52</span></td>
                                <td class="team text-center"> - </td>
                                <td class="team"><span id="OB2">Jacopo Guastella 56.22</span></td>
                            </tr>

                            <tr>
                                <td class="team text-right text-success"><span id="OC2">Ludovica Ciufoli 60.32</span></td>
                                <td class="team text-center"> - </td>
                                <td class="team"><span id="OD3">Alessandro Rastelli 53.03</span></td>
                            </tr>

                            <tr>
                                <td class="team text-right"><span id="OA4">Dario La Padula 55.40</span></td>
                                <td class="team text-center"> - </td>
                                <td class="team text-success"><span id="OB1">Mirko P. 60.13</span></td>
                            </tr>

                            <tr>
                                <td class="team text-right text-success"><span id="OC1">Andrea Frizziero 62.35</span></td>
                                <td class="team text-center"> - </td>
                                <td class="team"><span id="OD4">Federico Ciufoli 48.82</span></td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="standing">
                    <h3>Gironi</h3>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="standing_wrapper">
                                <table>
                                    <thead>
                                        <tr><th colspan="3"><div class="bold text-18 text-center">A</div></th>
                                    </tr></thead>
                                    <tbody>
                                        <tr id="A1">
                                            <td class="rank text-success bold">1</td>
                                            <td class="team text-success bold">Gozer il Gozeriano</td>
                                            <td class="points text-success bold">54.04</td>
                                        </tr>
                                        <tr id="A2">
                                            <td class="rank text-success bold">2</td>
                                            <td class="team text-success bold">Manolo</td>
                                            <td class="points text-success bold">48.72</td>
                                        </tr>
                                        <tr id="A3">
                                            <td class="rank text-success bold">3</td>
                                            <td class="team text-success bold">Alessandro zaffini</td>
                                            <td class="points text-success bold">48.20</td>
                                        </tr>
                                        <tr id="A4">
                                            <td class="rank text-success bold">4</td>
                                            <td class="team text-success bold">Dario La Padula</td>
                                            <td class="points text-success bold">47.68</td>
                                        </tr>
                                        <tr id="A5">
                                            <td class="rank ">5</td>
                                            <td class="team ">Danilo C.</td>
                                            <td class="points ">44.40</td>
                                        </tr>
                                        <tr id="A6">
                                            <td class="rank ">6</td>
                                            <td class="team ">Mattew</td>
                                            <td class="points ">44.30</td>
                                        </tr>
                                        <tr id="A7">
                                            <td class="rank ">7</td>
                                            <td class="team ">Matteo Cugini</td>
                                            <td class="points ">37.47</td>
                                        </tr>
                                        <tr id="A8">
                                            <td class="rank ">8</td>
                                            <td class="team ">Giulia Miele</td>
                                            <td class="points ">35.32</td>
                                        </tr>
                                        <tr id="A9">
                                            <td class="rank ">9</td>
                                            <td class="team ">Gianluca Losito</td>
                                            <td class="points ">31.66</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="standing_wrapper">
                                <table>
                                    <thead>
                                        <tr><th colspan="3"><div class="bold text-18 text-center">B</div></th>
                                    </tr></thead>
                                    <tbody>
                                        <tr id="B1">
                                            <td class="rank text-success bold">1</td>
                                            <td class="team text-success bold">Mirko P.</td>
                                            <td class="points text-success bold">51.49</td>
                                        </tr>
                                        <tr id="B2">
                                            <td class="rank text-success bold">2</td>
                                            <td class="team text-success bold">Jacopo Guastella</td>
                                            <td class="points text-success bold">49.89</td>
                                        </tr>
                                        <tr id="B3">
                                            <td class="rank text-success bold">3</td>
                                            <td class="team text-success bold">Enrico</td>
                                            <td class="points text-success bold">45.22</td>
                                        </tr>
                                        <tr id="B4">
                                            <td class="rank text-success bold">4</td>
                                            <td class="team text-success bold">Fabio Gradasso</td>
                                            <td class="points text-success bold">42.08</td>
                                        </tr>
                                        <tr id="B5">
                                            <td class="rank ">5</td>
                                            <td class="team ">Gabriele Tascioni</td>
                                            <td class="points ">39.05</td>
                                        </tr>
                                        <tr id="B6">
                                            <td class="rank ">6</td>
                                            <td class="team ">Claudio Massarotti</td>
                                            <td class="points ">37.83</td>
                                        </tr>
                                        <tr id="B7">
                                            <td class="rank ">7</td>
                                            <td class="team ">Leonardo Contreas</td>
                                            <td class="points ">35.32</td>
                                        </tr>
                                        <tr id="B8">
                                            <td class="rank ">8</td>
                                            <td class="team ">Alessandra Medda</td>
                                            <td class="points ">32.34</td>
                                        </tr>
                                        <tr id="B9">
                                            <td class="rank ">9</td>
                                            <td class="team ">William Ricca</td>
                                            <td class="points ">15.00</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="standing_wrapper">
                                <table>
                                    <thead>
                                        <tr><th colspan="3"><div class="bold text-18 text-center">C</div></th>
                                    </tr></thead>
                                    <tbody>
                                        <tr id="C1">
                                            <td class="rank text-success bold">1</td>
                                            <td class="team text-success bold">Andrea Frizziero</td>
                                            <td class="points text-success bold">53.20</td>
                                        </tr>
                                        <tr id="C2">
                                            <td class="rank text-success bold">2</td>
                                            <td class="team text-success bold">Ludovica Ciufoli</td>
                                            <td class="points text-success bold">50.58</td>
                                        </tr>
                                        <tr id="C3">
                                            <td class="rank text-success bold">3</td>
                                            <td class="team text-success bold">Salvatore lavorgna</td>
                                            <td class="points text-success bold">45.85</td>
                                        </tr>
                                        <tr id="C4">
                                            <td class="rank text-success bold">4</td>
                                            <td class="team text-success bold">Davide S.</td>
                                            <td class="points text-success bold">43.35</td>
                                        </tr>
                                        <tr id="C5">
                                            <td class="rank ">5</td>
                                            <td class="team ">Riccardo T.</td>
                                            <td class="points ">41.97</td>
                                        </tr>
                                        <tr id="C6">
                                            <td class="rank ">6</td>
                                            <td class="team ">Davide Mascolo</td>
                                            <td class="points ">38.09</td>
                                        </tr>
                                        <tr id="C7">
                                            <td class="rank ">7</td>
                                            <td class="team ">Emanuele Chieffo</td>
                                            <td class="points ">22.15</td>
                                        </tr>
                                        <tr id="C8">
                                            <td class="rank ">8</td>
                                            <td class="team ">Loris Di Battista</td>
                                            <td class="points ">6.03</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="standing_wrapper">
                                <table>
                                    <thead>
                                        <tr><th colspan="3"><div class="bold text-18 text-center">D</div></th>
                                    </tr></thead>
                                    <tbody>
                                        <tr id="D1">
                                            <td class="rank text-success bold">1</td>
                                            <td class="team text-success bold">Massimo Palozzi</td>
                                            <td class="points text-success bold">50.44</td>
                                        </tr>
                                        <tr id="D2">
                                            <td class="rank text-success bold">2</td>
                                            <td class="team text-success bold">Fabrizio Di Coste</td>
                                            <td class="points text-success bold">48.00</td>
                                        </tr>
                                        <tr id="D3">
                                            <td class="rank text-success bold">3</td>
                                            <td class="team text-success bold">Alessandro Rastelli</td>
                                            <td class="points text-success bold">45.51</td>
                                        </tr>
                                        <tr id="D4">
                                            <td class="rank text-success bold">4</td>
                                            <td class="team text-success bold">Federico Ciufoli</td>
                                            <td class="points text-success bold">45.31</td>
                                        </tr>
                                        <tr id="D5">
                                            <td class="rank ">5</td>
                                            <td class="team ">Ale R</td>
                                            <td class="points ">42.46</td>
                                        </tr>
                                        <tr id="D6">
                                            <td class="rank ">6</td>
                                            <td class="team ">Gastone</td>
                                            <td class="points ">42.22</td>
                                        </tr>
                                        <tr id="D7">
                                            <td class="rank ">7</td>
                                            <td class="team ">Eugenio Congiu</td>
                                            <td class="points ">38.69</td>
                                        </tr>
                                        <tr id="D8">
                                            <td class="rank ">8</td>
                                            <td class="team ">Francesco</td>
                                            <td class="points ">16.75</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>

                    </div>
                </div>

            </div>

            <div class="col-xs-12 col-md-4 col-lg-3 standing">
                <h3>Classifica Generale</h3>
                <div class="standing_wrapper">
                    <table>
                        <tbody>

                        <tr>
                            <td class="rank text-success">1</td>
                            <td class="team text-success"><a href="http://giocopronostici.000webhostapp.com/auth/results/26">
                                    <span class="text-success">Gozer il Gozeriano</span>
                                </a></td>
                            <td class="points text-success">96.82</td>
                        </tr>

                        <tr>
                            <td class="rank text-info">2</td>
                            <td class="team text-info"><a href="http://giocopronostici.000webhostapp.com/auth/results/24">
                                    <span class="text-info">Ludovica Ciufoli</span>
                                </a></td>
                            <td class="points text-info">71.31</td>
                        </tr>

                        <tr>
                            <td class="rank text-info">3</td>
                            <td class="team text-info"><a href="http://giocopronostici.000webhostapp.com/auth/results/34">
                                    <span class="text-info">Manolo</span>
                                </a></td>
                            <td class="points text-info">69.71</td>
                        </tr>
                        <tr>
                            <td class="rank ">4</td>
                            <td class="team "><a href="http://giocopronostici.000webhostapp.com/auth/results/3">
                                    <span class="">Mirko P.</span>
                                </a></td>
                            <td class="points ">65.46</td>
                        </tr>
                        <tr>
                            <td class="rank ">5</td>
                            <td class="team "><a href="http://giocopronostici.000webhostapp.com/auth/results/28">
                                    <span class="">Andrea Frizziero</span>
                                </a></td>
                            <td class="points ">65.30</td>
                        </tr>
                        <tr>
                            <td class="rank ">6</td>
                            <td class="team "><a href="http://giocopronostici.000webhostapp.com/auth/results/25">
                                    <span class="">Massimo Palozzi</span>
                                </a></td>
                            <td class="points ">65.21</td>
                        </tr>
                        <tr>
                            <td class="rank ">7</td>
                            <td class="team "><a href="http://giocopronostici.000webhostapp.com/auth/results/1">
                                    <span class="">Dario La Padula</span>
                                </a></td>
                            <td class="points ">63.72</td>
                        </tr>
                        <tr>
                            <td class="rank ">8</td>
                            <td class="team "><a href="http://giocopronostici.000webhostapp.com/auth/results/2">
                                    <span class="">Danilo C.</span>
                                </a></td>
                            <td class="points ">63.70</td>
                        </tr>
                        <tr>
                            <td class="rank ">9</td>
                            <td class="team "><a href="http://giocopronostici.000webhostapp.com/auth/results/12">
                                    <span class="">Salvatore lavorgna</span>
                                </a></td>
                            <td class="points ">63.06</td>
                        </tr>
                        <tr>
                            <td class="rank ">10</td>
                            <td class="team "><a href="http://giocopronostici.000webhostapp.com/auth/results/35">
                                    <span class="">Fabio Gradasso</span>
                                </a></td>
                            <td class="points ">61.46</td>
                        </tr>
                        <tr>
                            <td class="rank ">11</td>
                            <td class="team "><a href="http://giocopronostici.000webhostapp.com/auth/results/7">
                                    <span class="">Alessandro Rastelli</span>
                                </a></td>
                            <td class="points ">59.95</td>
                        </tr>
                        <tr>
                            <td class="rank ">12</td>
                            <td class="team "><a href="http://giocopronostici.000webhostapp.com/auth/results/22">
                                    <span class="">Alessandro zaffini</span>
                                </a></td>
                            <td class="points ">58.47</td>
                        </tr>
                        <tr>
                            <td class="rank ">13</td>
                            <td class="team "><a href="http://giocopronostici.000webhostapp.com/auth/results/23">
                                    <span class="">Enrico</span>
                                </a></td>
                            <td class="points ">57.21</td>
                        </tr>
                        <tr>
                            <td class="rank ">14</td>
                            <td class="team "><a href="http://giocopronostici.000webhostapp.com/auth/results/8">
                                    <span class="">Jacopo Guastella</span>
                                </a></td>
                            <td class="points ">56.22</td>
                        </tr>
                        <tr>
                            <td class="rank ">15</td>
                            <td class="team "><a href="http://giocopronostici.000webhostapp.com/auth/results/14">
                                    <span class="">Fabrizio Di Coste</span>
                                </a></td>
                            <td class="points ">54.10</td>
                        </tr>
                        <tr>
                            <td class="rank ">16</td>
                            <td class="team "><a href="http://giocopronostici.000webhostapp.com/auth/results/20">
                                    <span class="">Davide S.</span>
                                </a></td>
                            <td class="points ">50.27</td>
                        </tr>
                        <tr>
                            <td class="rank ">17</td>
                            <td class="team "><a href="http://giocopronostici.000webhostapp.com/auth/results/4">
                                    <span class="">Federico Ciufoli</span>
                                </a></td>
                            <td class="points ">48.82</td>
                        </tr>
                        <tr>
                            <td class="rank ">18</td>
                            <td class="team "><a href="http://giocopronostici.000webhostapp.com/auth/results/16">
                                    <span class="">Eugenio Congiu</span>
                                </a></td>
                            <td class="points ">48.80</td>
                        </tr>
                        <tr>
                            <td class="rank ">19</td>
                            <td class="team "><a href="http://giocopronostici.000webhostapp.com/auth/results/10">
                                    <span class="">Claudio Massarotti</span>
                                </a></td>
                            <td class="points ">48.10</td>
                        </tr>
                        <tr>
                            <td class="rank ">20</td>
                            <td class="team "><a href="http://giocopronostici.000webhostapp.com/auth/results/15">
                                    <span class="">Matteo Cugini</span>
                                </a></td>
                            <td class="points ">46.78</td>
                        </tr>
                        <tr>
                            <td class="rank ">21</td>
                            <td class="team "><a href="http://giocopronostici.000webhostapp.com/auth/results/33">
                                    <span class="">Ale R</span>
                                </a></td>
                            <td class="points ">46.71</td>
                        </tr>
                        <tr>
                            <td class="rank ">22</td>
                            <td class="team "><a href="http://giocopronostici.000webhostapp.com/auth/results/21">
                                    <span class="">Mattew</span>
                                </a></td>
                            <td class="points ">44.30</td>
                        </tr>
                        <tr>
                            <td class="rank ">23</td>
                            <td class="team "><a href="http://giocopronostici.000webhostapp.com/auth/results/29">
                                    <span class="">Gastone</span>
                                </a></td>
                            <td class="points ">42.22</td>
                        </tr>
                        <tr>
                            <td class="rank ">24</td>
                            <td class="team "><a href="http://giocopronostici.000webhostapp.com/auth/results/9">
                                    <span class="">Riccardo T.</span>
                                </a></td>
                            <td class="points ">41.97</td>
                        </tr>
                        <tr>
                            <td class="rank ">25</td>
                            <td class="team "><a href="http://giocopronostici.000webhostapp.com/auth/results/6">
                                    <span class="">Gabriele Tascioni</span>
                                </a></td>
                            <td class="points ">41.43</td>
                        </tr>
                        <tr>
                            <td class="rank ">26</td>
                            <td class="team "><a href="http://giocopronostici.000webhostapp.com/auth/results/31">
                                    <span class="">Alessandra Medda</span>
                                </a></td>
                            <td class="points ">41.19</td>
                        </tr>
                        <tr>
                            <td class="rank ">27</td>
                            <td class="team "><a href="http://giocopronostici.000webhostapp.com/auth/results/17">
                                    <span class="">Davide Mascolo</span>
                                </a></td>
                            <td class="points ">38.09</td>
                        </tr>
                        <tr>
                            <td class="rank ">28</td>
                            <td class="team "><a href="http://giocopronostici.000webhostapp.com/auth/results/5">
                                    <span class="">Leonardo Contreas</span>
                                </a></td>
                            <td class="points ">37.42</td>
                        </tr>
                        <tr>
                            <td class="rank ">29</td>
                            <td class="team "><a href="http://giocopronostici.000webhostapp.com/auth/results/30">
                                    <span class="">Giulia Miele</span>
                                </a></td>
                            <td class="points ">35.32</td>
                        </tr>
                        <tr>
                            <td class="rank ">30</td>
                            <td class="team "><a href="http://giocopronostici.000webhostapp.com/auth/results/19">
                                    <span class="">Gianluca Losito</span>
                                </a></td>
                            <td class="points ">31.66</td>
                        </tr>
                        <tr>
                            <td class="rank ">31</td>
                            <td class="team "><a href="http://giocopronostici.000webhostapp.com/auth/results/32">
                                    <span class="">Emanuele Chieffo</span>
                                </a></td>
                            <td class="points ">28.37</td>
                        </tr>
                        <tr>
                            <td class="rank ">32</td>
                            <td class="team "><a href="http://giocopronostici.000webhostapp.com/auth/results/18">
                                    <span class="">Francesco</span>
                                </a></td>
                            <td class="points ">16.75</td>
                        </tr>
                        <tr>
                            <td class="rank ">33</td>
                            <td class="team "><a href="http://giocopronostici.000webhostapp.com/auth/results/27">
                                    <span class="">William Ricca</span>
                                </a></td>
                            <td class="points ">15.00</td>
                        </tr>
                        <tr>
                            <td class="rank ">34</td>
                            <td class="team "><a href="http://giocopronostici.000webhostapp.com/auth/results/37">
                                    <span class="">Emanuele DG</span>
                                </a></td>
                            <td class="points ">10.76</td>
                        </tr>
                        <tr>
                            <td class="rank ">35</td>
                            <td class="team "><a href="http://giocopronostici.000webhostapp.com/auth/results/13">
                                    <span class="">Loris Di Battista</span>
                                </a></td>
                            <td class="points ">6.03</td>
                        </tr>
                        </tbody>
                    </table>

                    <h3 class="standing">Premi della critica</h3>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="statistics text-18">

                                <p><strong>Miglior quota indovinata</strong>:<br>
                                    Gozer il Gozeriano: 21.00 con Croazia - Inghilterra Marcatore: Kieran Trippier<br>
                                </p>

                                <p><strong>Più eventi indovinati</strong>:<br>
                                    Ludovica Ciufoli: 39<br>
                                </p>
                                <p><strong>Miglior punteggio femminile</strong>:<br>
                                    Ludovica Ciufoli: 71.31
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

</div>
<!-- /.box -->
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop
