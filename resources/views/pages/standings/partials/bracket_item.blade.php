<div class="row mt-3">
    <div class="col-5 text-right">
        <span class="text-14">{{ $player1 }}</span>
        <span class="badge {{ $score1 > $score2 ? 'bg-success' : ($score1 == $score2 ? 'bg-primary' : 'bg-danger') }}">{{ $score1 }}</span>
    </div>
    <div class="col-2 text-center">
        <span class="bold">VS</span>
    </div>
    <div class="col-5 text-left">
        <span class="badge {{ $score1 < $score2 ? 'bg-success' : ($score1 == $score2 ? 'bg-primary' : 'bg-danger') }}">{{ $score2 }}</span>
        <span class="text-14">{{ $player2 }}</span>
    </div>
</div>

