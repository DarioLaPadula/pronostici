<?php

use Illuminate\Support\Carbon ?>

{{-- resources/views/admin/dashboard.blade.php --}}
@extends('page')

@section('content_header')
    <h1>{{ config('app.name') }} - Classifiche {{ $tournament->name }}</h1>
@stop

@section('content')
    @parent

    <!--    <small>(Ultimo aggiornamento:  // Carbon::parse($lastUpdate)->format('d/m/Y H:i') )</small>-->

    <div class="card card-primary card-outline card-outline-tabs">
        <div class="card-header p-0 border-bottom-0">
            <ul class="nav nav-tabs" id="standings-tab" role="tablist">
                @if (!$knockoutPhases->isEmpty())
                    <li class="nav-item">
                        <a class="nav-link active" id="knockout-tab" data-toggle="pill" href="#knockout" role="tab"
                           aria-controls="knockout" aria-selected="false">Eliminazione Diretta</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="general-tab" data-toggle="pill" href="#general" role="tab"
                           aria-controls="general" aria-selected="false">
                            {{ !$points->isEmpty() ? 'Classifica Generale' : 'Elenco Iscritti' }}</a>
                    </li>

                    @if ($groupsPhases)
                        <li class="nav-item">
                            <a class="nav-link" id="groups-tab" data-toggle="pill" href="#groups" role="tab"
                               aria-controls="groups" aria-selected="false">
                                    Fasi precedenti
                            </a>
                        </li>
                    @endif
                    {{--<li><a href="#draw" data-toggle="tab">Sorteggi</a></li> @TODO --}}
                @else
                    @if (!$groupsPhases->isEmpty())
                        <li class="nav-item">
                            <a class="nav-link active" id="groups-tab" data-toggle="pill" href="#groups" role="tab"
                               aria-controls="groups" aria-selected="true">
                                @foreach($groupsPhases as $key => $gp)
                                    {{ $gp->name }}@if ($key > 0) - @endif
                                @endforeach</a>
                        </li>
                    @endif

                    <li class="nav-item">
                        <a class="nav-link" id="general-tab" data-toggle="pill" href="#general"
                            role="tab" aria-controls="general" aria-selected="true">{{ !$points->isEmpty() ? 'Classifica Generale' : 'Elenco Iscritti' }}</a>
                    </li>
                    {{--<li><a href="#draw" data-toggle="tab">Sorteggi</a></li> @TODO --}}
                @endif
            </ul>
        </div>
        <div class="card-body">
            <div class="tab-content" id="standings-tabContent">
                @if (!$knockoutPhases->isEmpty())
                    @include('pages.standings.partials.knockout')
                @endif

                @if (!$groupsPhases->isEmpty())
                    @include('pages.standings.partials.groups')
                @endif

                @include('pages.standings.partials.general')
            </div>
        </div>
        <!-- /.card -->
    </div>
    <!-- /.box -->
@stop
@include('partials.datatable')
