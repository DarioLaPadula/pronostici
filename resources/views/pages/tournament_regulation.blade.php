@extends('page')

@section('content_header')
    <h1>{{ $news->title }}</h1>
@stop

@section('content')
    @parent

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            {!! $news->news !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop

