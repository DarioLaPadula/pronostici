@if (isset($surveys))
@section('adminlte_css')
    @parent
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/plugins/iCheck/square/blue.css') }}">
@stop

@section('content')
    @foreach($surveys as $survey)
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3>{{ $survey->message }}</h3>
                    </div>
                    <div class="box-body">
                        <form action="{{ url('auth/survey') }}" method="post">
                            {!! csrf_field() !!}
                            @foreach ($survey->survey_choice as $choice)
                                <div class="form-group">
                                    <input type="checkbox" name="survey_result[{{ $choice->id }}]"
                                           id="choice[{{ $choice->id }}]">
                                    <label style="margin-left: 10px;"
                                           for="survey_result[{{ $choice->id }}]">{{ $choice->response }}</label>
                                </div>

                            @endforeach

                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-3 offset-md-9 col-xs-12 pull-right m-t">
                                        <button type="submit"
                                                class="form-control btn btn-primary btn-block btn-flat">{{ trans('custom.send') }}</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection

@section('js')
    <script src="{{ asset('vendor/adminlte/plugins/iCheck/icheck.min.js') }}"></script>

    <script>
        $(document).ready(function () {
            $('button').click(function () {
                checked = $("input[type=checkbox]:checked").length;

                if (!checked) {
                    alert("Seleziona almeno un valore");
                    return false;
                }

            });
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>

@endsection
@endif