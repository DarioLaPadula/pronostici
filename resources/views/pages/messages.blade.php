<?php
use Illuminate\Support\Carbon ?>

{{-- resources/views/admin/dashboard.blade.php --}}

@extends('page')

@section('content')
@parent
<h1>Gioco Pronostici - Messaggi</h1>

<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary direct-chat direct-chat-primary">
            <div class="box-header with-border">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <!-- Conversations are loaded here -->

                <div class="direct-chat-messages">
                    <!-- Message. Default to the left -->
                    @foreach($messages as $message)
                    <div class="direct-chat-msg {{ Auth::user()->id == $message->user->id ? ' right' : ''}}">
                        <div class="direct-chat-info clearfix">
                            <span class="direct-chat-name pull-left">{{ $message->user->name }}</span>
                            <span class="direct-chat-timestamp pull-right">{{ Carbon::parse($message->created_at)->format('d/m/Y H:i') }}</span>
                        </div>
                        <div class="direct-chat-text">
                            {{ $message->message }}
                        </div>

                    </div>

                    @endforeach


                </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <form action="{{ url('auth/messages/' . $tournamentId) }}" method="post">
                    {!! csrf_field() !!}
                    <div class="input-group">
                        <input type="text" name="message" placeholder="{{ trans('custom.write_message') }}" class="form-control" required>
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-success btn-flat">Send</button>
                        </span>
                    </div>
                </form>
            </div>
            <!-- /.box-footer-->
        </div>
    </div>
</div><!--/.col-->



@stop
