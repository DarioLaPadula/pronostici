<div class="form-row" data-number="{{ $key }}">

    <div class="col-sm-5">
        <label for="best_odd_win_user_ids">{{ __('hall_of_fame.best_odd_win_users') }}</label>
        <x-adminlte-select2 name="best_odd_wins[{{ $key }}][user_ids][]" class="selectize bet_odd_wins_users" multiple>
            @foreach ($users as $user)
                <option value="{{ $user->id }}" {{ in_array($user->id, old('best_odd_wins.' . $key . '.user_ids', isset($value['user_ids']) ? $value['user_ids'] : [])) ? 'selected' : '' }}>
                    {{ $user->name }}
                </option>
            @endforeach
        </x-adminlte-select2>
    </div>

    <div class="col-sm-5">
        <label for="best_odd_wins_{{ $key }}_odd">{{ __('hall_of_fame.best_odd_win_description') }}</label>
        <input type="text" name="best_odd_wins[{{ $key }}][description]" class="form-control bet_odd_wins_description"
               value="{{ old('best_odd_wins.' . $key . '.description', isset($value['description']) ? $value['description'] : null) }}">
    </div>

    <div class="col-sm-1">
        <label>&nbsp;</label>
        <button type="button"
                class="form-control btn btn-primary btn-block btn-flat btn-plus">+
        </button>
    </div>
    <div class="col-sm-1">
        <label>&nbsp;</label>
        <button type="button"
                class="form-control btn btn-danger btn-block btn-flat btn-minus">-
        </button>
    </div>
</div>
