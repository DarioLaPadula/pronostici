@extends('page')

@section('content_header')
    <h1>Admin - Gestione albo D'oro</h1>
@endsection

@section('title', 'Gestione Albo d\'oro')

@section('content')
    @parent

    <div class="card">
        <div class="card-header">

            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif

            @if(Session::has('message'))
                <div class="alert alert-danger">
                    {{ Session::get('message') }}
                </div>
            @endif

            <a href="{{ route('admin.hall_of_fames.create') }}"
               class="btn btn-primary float-right">{{ __('custom.create_new') }}</a>
        </div>

        <div class="card-body">

            <table class="table datatable responsive">
                <thead>
                <tr class="text-center">
                    <th class="border-top-0">{{ __('hall_of_fame.id') }}</th>
                    <th class="border-top-0">{{ __('hall_of_fame.tournament') }}</th>
                    <th class="border-top-0">{{ __('hall_of_fame.first_users') }}</th>
                    <th class="border-top-0">{{ __('hall_of_fame.second_users') }}</th>
                    <th class="border-top-0">{{ __('hall_of_fame.third_users') }}</th>
                    <th class="border-top-0">{{ __('hall_of_fame.first_points') }}</th>
                    <th class="border-top-0">{{ __('hall_of_fame.total_points_first') }}</th>
                    <th class="border-top-0">{{ __('hall_of_fame.second_points') }}</th>
                    <th class="border-top-0">{{ __('hall_of_fame.total_points_second') }}</th>
                    <th class="border-top-0">{{ __('hall_of_fame.third_points') }}</th>
                    <th class="border-top-0">{{ __('hall_of_fame.total_points_third') }}</th>
                    <th class="border-top-0">{{ __('hall_of_fame.best_odd_win_users') }}</th>
                    <th class="border-top-0">{{ __('hall_of_fame.total_best_odd_win') }}</th>
                    <th class="border-top-0">{{ __('hall_of_fame.high_prediction_win_users') }}</th>
                    <th class="border-top-0">{{ __('hall_of_fame.total_high_prediction_win') }}</th>
                    <th class="border-top-0">{{ __('hall_of_fame.actions') }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($hallOfFames as $hallOfFame)
                    <tr class="text-center">
                        <td>{{ $hallOfFame->id }}</td>
                        <td>{{ $hallOfFame->tournament->name }}</td>
                        <td>
                            @foreach ($hallOfFame->firstUsers() as $key => $user)
                                {{ $user->name . (!$loop->last ? ', ' : '') }}
                            @endforeach
                        </td>
                        <td>
                            @foreach ($hallOfFame->secondUsers() as $key => $user)
                                {{ $user->name . (!$loop->last ? ', ' : '') }}
                            @endforeach
                        </td>
                        <td>
                            @foreach ($hallOfFame->thirdUsers() as $key => $user)
                                {{ $user->name . (!$loop->last ? ', ' : '') }}
                            @endforeach
                        </td>
                        <td>
                            @foreach ($hallOfFame->firstPointsUsers() as $key => $user)
                                {{ $user->name . (!$loop->last ? ', ' : '') }}
                            @endforeach
                        </td>
                        <td>
                            {{ number_format($hallOfFame->total_points_first, 2) }}
                        </td>
                        <td>
                            @foreach ($hallOfFame->secondPointsUsers() as $key => $user)
                                {{ $user->name . (!$loop->last ? ', ' : '') }}
                            @endforeach
                        </td>
                        <td>
                            {{ number_format($hallOfFame->total_points_second, 2) }}
                        </td>
                        <td>
                            @foreach ($hallOfFame->thirdPointsUsers() as $key => $user)
                                {{ $user->name . (!$loop->last ? ', ' : '') }}
                            @endforeach
                        </td>
                        <td>
                            {{ number_format($hallOfFame->total_points_third, 2) }}
                        </td>
                        <td>
                            @foreach ($hallOfFame->best_odd_wins as $value)
                                @foreach ($value['user_ids'] as $user_id)
                                    {{ $bestOddWinUsers[$user_id] . (!$loop->last ? ', ' : '') }}
                                @endforeach
                                {{ __('custom.with') }} {{ $value['description'] }}<br><br>
                            @endforeach
                        </td>
                        <td>
                            {{ $hallOfFame->total_best_odd_win }}
                        </td>
                        <td>
                            @foreach ($hallOfFame->highPredictionWinUsers() as $key => $user)
                                {{ $user->name . (!$loop->last ? ', ' : '') }}
                            @endforeach
                        </td>
                        <td>
                            {{ $hallOfFame->total_high_prediction_win }}
                        </td>
                        <td class="text-center">

                            <div class="btn-group" role="group">

                                    <a class="btn btn-primary mr-2" href="{{ route('admin.hall_of_fames.edit', $hallOfFame->id) }}">
                                        <i class="fas fa-edit"></i>
                                    </a>

                                    <form action="{{ route('admin.hall_of_fames.destroy', $hallOfFame->id) }}"
                                          method="POST" style="margin-block-end: 0">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger delete-hall-of-fame-btn"
                                                data-id="{{ $hallOfFame->id }}">
                                            <i class="fas fa-trash"></i>
                                        </button>
                                    </form>
                            </div>

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @include('partials.datatable')
@endsection
@section('plugins.Sweetalert2', true)
@section('js')
    <script>
        $(document).ready(function () {
            $('.delete-hall-of-fame-btn').click(function (e) {
                e.preventDefault(); // Prevent default form submission

                Swal.fire({
                    title: '{{ __('custom.are_you_sure') }}',
                    text: '{{ __('custom.cannot_undo_this_action') }}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '{{ __('custom.delete') }}',
                    cancelButtonText: '{{ __('custom.no') }}',
                }).then((result) => {
                    if (result.isConfirmed) {
                       $(e.target).closest('form').submit();
                    }
                });
            });
        });
    </script>
@endsection
