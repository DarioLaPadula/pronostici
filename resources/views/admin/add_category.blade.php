@extends('page')

@section('css')
    @parent
@stop

@section('content_header')
    <h1>Admin - Aggiungi Categoria</h1>
@stop

@section('content')
    @parent

    @include('partials.messages')

    <form action="{{ route('admin.categories.store') }}" method="post">
        {!! csrf_field() !!}

        <div class="card">
            <div class="card-body">

                @if(session()->has('admin.category.add'))
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="alert alert-{{ session('admin.tournament.add') }}">
                                {!! session('admin.category.add') !!}
                            </div>
                        </div>
                    </div>
                @endif

                <div class="row">
                    <div class="col-xs-12 col-md-4">
                        <div class="form-group has-feedback {{ $errors->has('name') ? 'has-error' : '' }}">
                            <input type="text" name="name" class="form-control"
                                   placeholder="{{ trans('custom.name') }}" required>
                            @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <div class="form-group">

                            <button type="submit"
                                    class="form-control btn btn-primary btn-block btn-flat">{{ trans('custom.save') }}</button>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

@stop
