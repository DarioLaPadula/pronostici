{{-- resources/views/admin/dashboard.blade.php --}}

@extends('page')

@section('content_header')
    <h1>Sorteggio</h1>
@stop

@section('content')
    <div class="card">
        <div class="card-body bg-dark">
            <div class="row">
                <div class="col-xs-12 col-md-9">
                    <div class="row mt-3">
                        @foreach ($users as $key => $pot)
                            <div class="col-md-4 mt-3"><h4 class="text-success">{{ $key }}</h4>
                                <div class="row pot">
                                    @foreach ($pot as $key => $user)

                                        <div class="@if (count($pot) > 8) col-md-6 @endif col-12 mt-3">
                                            <p class="user {{ $key }}" id="{{$user}}" style="font-size: 18px">
                                                <strong>{{$user}}</strong>
                                            </p>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="col-xs-12 col-md-3">
                    <div class="row">
                        <div class="col-md-6 a">
                            <div class="bold text-lg">A</div>
                            <p class="text-center 1">&nbsp;</p>
                            <p class="text-center 2">&nbsp;</p>
                            <p class="text-center 3">&nbsp;</p>
                            <p class="text-center 4">&nbsp;</p>
                            <p class="text-center 5">&nbsp;</p>
                            <p class="text-center 6">&nbsp;</p>
                        </div>
                        <div class="col-md-6 b">
                            <div class="bold text-lg">B</div>
                            <p class="text-center 1">&nbsp;</p>
                            <p class="text-center 2">&nbsp;</p>
                            <p class="text-center 3">&nbsp;</p>
                            <p class="text-center 4">&nbsp;</p>
                            <p class="text-center 5">&nbsp;</p>
                            <p class="text-center 6">&nbsp;</p>
                        </div>
                        <div class="col-md-6 c mt-3">
                            <div class="bold text-lg">C</div>
                            <p class="text-center 1">&nbsp;</p>
                            <p class="text-center 2">&nbsp;</p>
                            <p class="text-center 3">&nbsp;</p>
                            <p class="text-center 4">&nbsp;</p>
                            <p class="text-center 5">&nbsp;</p>
                            <p class="text-center 6">&nbsp;</p>
                        </div>
                        <div class="col-md-6 d mt-3">
                            <div class="bold text-lg">D</div>
                            <p class="text-center 1">&nbsp;</p>
                            <p class="text-center 2">&nbsp;</p>
                            <p class="text-center 3">&nbsp;</p>
                            <p class="text-center 4">&nbsp;</p>
                            <p class="text-center 5">&nbsp;</p>
                            <p class="text-center 6">&nbsp;</p>
                        </div>
                        <div class="col-md-6 e mt-3">
                            <div class="bold text-lg">E</div>
                            <p class="text-center 1">&nbsp;</p>
                            <p class="text-center 2">&nbsp;</p>
                            <p class="text-center 3">&nbsp;</p>
                            <p class="text-center 4">&nbsp;</p>
                            <p class="text-center 5">&nbsp;</p>
                            <p class="text-center 6">&nbsp;</p>
                        </div>
                        <div class="col-md-6 f mt-3">
                            <div class="bold text-lg">F</div>
                            <p class="text-center 1">&nbsp;</p>
                            <p class="text-center 2">&nbsp;</p>
                            <p class="text-center 3">&nbsp;</p>
                            <p class="text-center 4">&nbsp;</p>
                            <p class="text-center 5">&nbsp;</p>
                            <p class="text-center 6">&nbsp;</p>
                        </div>
                        <div class="col-md-6 g mt-3">
                            <div class="bold text-lg">G</div>
                            <p class="text-center 1">&nbsp;</p>
                            <p class="text-center 2">&nbsp;</p>
                            <p class="text-center 3">&nbsp;</p>
                            <p class="text-center 4">&nbsp;</p>
                            <p class="text-center 5">&nbsp;</p>
                            <p class="text-center 6">&nbsp;</p>
                        </div>
                        <div class="col-md-6 h mt-3">
                            <div class="bold text-lg">H</div>
                            <p class="text-center 1">&nbsp;</p>
                            <p class="text-center 2">&nbsp;</p>
                            <p class="text-center 3">&nbsp;</p>
                            <p class="text-center 4">&nbsp;</p>
                            <p class="text-center 5">&nbsp;</p>
                            <p class="text-center 6">&nbsp;</p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('js')
<!--    <script>

        var users = [];

        function findUser($pot, groups) {
            var random = Math.floor(Math.random() * $pot.find('.user').length);

            var user = $pot.find(".user").eq(random);

            if (user.hasClass(groups)) {
                return findUser($pot);
            }

            return user;

        }

        function draw(pot, potNo) {
            var groups = 1;
            // var potNo = 0;
            var time = 1000;
            var timer;

            while (groups <= 8) {
                var $pot = $('.pot').eq(potNo);

                var user = findUser($pot, groups);

                var offset = $('.' + pot).find('.' + groups).offset();

                var posY = offset.top;
                var posX = offset.left + 40;

                users.push({user: user, posX: posX, posY: posY});
                user.removeClass('user');
                //

                //  potNo = 1;
                groups++;

            }
            //  potNo++;

        }

        draw('a', 0);
        draw('b', 1);

        var time = 2000;

        document.body.onkeyup = function (e) {
            if (e.keyCode == 32) {
                // setTimeout(function () {
                $(users[0].user).css({
                    position: 'fixed'
                }).animate({top: users[0].posY, left: users[0].posX}, "slow");
                users.shift();
                //  }, time);
            }
        }

        /* $.each(users, function( index, value ) {

             console.log(value);

             setTimeout(function () {
                 $(value.user).css({
                     position: 'fixed'
                 }).animate({top: value.posY, left: value.posX}, "slow");
             }, time);
             time += 2000;

         });*/




        /* setTimeout(function () {
             while (groups <= 5) {

                 $.each(['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'], function (index, value) {

                     //console.log(potNo);
                     if (potNo > 2) {
                         var $pot = $('.pot').eq(3);
                     } else {
                         var $pot = $('.pot').eq(potNo);
                     }

                     console.log($pot.find('.user').length);

                     var random = Math.floor(Math.random() * $pot.find('.user').length);
                     var user = $pot.find(".user").eq(random);

                     var offset = $('.' + value).find('.' + groups).offset();

                     var posY = offset.top;
                     var posX = offset.left + 40;


                     user.removeClass('user');

                 });
                 //  potNo = 1;
                 groups++;
                 potNo++;
             }

         }, time);
         time += 2000;*/
    </script>-->


<script>
    var groups = 1;
    var potNo = 0;
    var time = 1000;
    setTimeout(function () {
        while (groups <= 6) {

            $.each(['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'], function (index, value) {

                //console.log(potNo);
                if (potNo > 3) {
                    var $pot = $('.pot').eq(4);
                } else {
                    var $pot = $('.pot').eq(potNo);
                }

                console.log($pot.find('.user').length);

                var random = Math.floor(Math.random() * $pot.find('.user').length);
                var user = $pot.find(".user").eq(random);

                var offset = $('.' + value).find('.' + groups).offset();

                var posY = offset.top;
                var posX = offset.left;

                setTimeout(function () {
                    $(user).css({
                        position: 'fixed'
                    }).animate({top: posY, left: posX}, "slow");
                }, time);
                time += 2000;

                user.removeClass('user');

            });
            //  potNo = 1;
            groups++;
            potNo++;
        }

    }, time);
    time += 2000;
</script>

@stop
