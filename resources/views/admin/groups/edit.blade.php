@extends('page')

@section('content_header')
    <h1>Admin - {{ __('custom.edit_tournament') }}</h1>
@stop

@section('css')
    @parent
    <link rel="stylesheet" type="text/css" href="{{ asset ('css/plugin/selectize/selectize.css') }}">
@stop


@section('title', 'Pronostici')

@section('content')
    @parent

    @include('partials.messages')

    <div class="card">
        <div class="card-body">

            <form action="{{ url(route('admin.groups.update', $group->id)) }}" method="post" autocomplete="off"
                  enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="form-group">
                    <div class="row">
                        <div class="col-12">
                            <label for="tournament_id">Torneo</label>
                            <select name="tournament_id" id="tournament_id" class="form-control" disabled readonly>
                                <option value="{{ $tournament->id }}">{{ $tournament->name }}</option>
                            </select>
                            <input type="hidden" name="tournament_id" value="{{ $tournament->id }}">
                        </div>
                    </div>
                </div>

                <div class="group-add">
                    <div class="form-group">
                        <div class="row group">
                            <div class="col-md-6 col-xs-12">
                                <label for="user_ids">Utenti</label>
                                <select name="user_ids[]" id="user_ids" class="selectize multiple" multiple required>
                                    <option value="">Seleziona...</option>
                                    @foreach ($users as $user)
                                        <option value="{{ $user->id }}"
                                                {{ (in_array($user->id, old('user_ids', $group->users->pluck('id')->toArray()))) ? 'selected' : '' }}>
                                            {{ $user->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-2 col-xs-12">
                                <label for="phase_id">Fase</label>
                                <select name="phase_id" id="phase_id" class="form-control" required>
                                    <option value="">Seleziona...</option>
                                    @foreach ($phases as $phase)
                                        <option value="{{ $phase->id }}"
                                                {{ (old('group.0.phase_id', $group->phase_id) == $phase->id) ? 'selected' : '' }}>
                                            {{ $phase->phase }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-2 col-xs-12">
                                <label for="group_0_group">Nome Gruppo</label>
                                <input type="text" name="group" id="group" class="form-control"
                                       placeholder="Gruppo" value="{{ old('group', $group->group) }}" required>
                            </div>

                            <div class="col-md-2 col-xs-12">
                                <label for="group_0_bonus">Bonus</label>
                                <input type="text" name="bonus" id="bonus" class="form-control"
                                       placeholder="Bonus" value="{{ old('bonus', $group->bonus) }}">
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3 offset-md-9 col-xs-12 pull-right m-t">
                        <button type="submit"
                                class="form-control btn btn-primary btn-block btn-flat">{{ trans('custom.save') }}</button>
                    </div>
                </div>

            </form>
        </div>
    </div>


@stop

@section('js')
    <script src="{{ asset('js/plugin/selectize.min.js') }}"></script>
    <script>
        $('.selectize').selectize({
            create: false,
        })
    </script>
@stop
