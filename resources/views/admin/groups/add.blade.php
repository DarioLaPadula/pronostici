@extends('page')

@section('content_header')
    <h1>Admin - Aggiungi Torneo</h1>
@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('css/plugin/jquery.datetimepicker.min.css') }}">
@stop


@section('title', 'Pronostici')

@section('content')
    @parent

    @include('partials.messages')

    <div class="card">
        <div class="card-body">

            <form action="{{ url(route('admin.groups.store')) }}" method="post" autocomplete="off"
                  enctype="multipart/form-data">
                {!! csrf_field() !!}

                <div class="form-group">
                    <div class="row">
                        <div class="col-12">
                            <label for="tournament_id">Torneo</label>
                            <select name="tournament_id" id="tournament_id" class="form-control" required>
                                <option value="">Seleziona torneo...</option>
                                @foreach ($tournaments as $tournament)
                                    <option value="{{ $tournament->id }}">
                                        {{ $tournament->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="group-add">
                    <div class="form-group">
                        <div class="row group">
                            <div class="col-md-3 col-xs-12">
                                <label for="group_0_users_ids">Utente</label>
                                <select name="group[0][user_id]" id="group_0_user_id" class="form-control" required>
                                    <option value="">Seleziona...</option>
                                    <!-- Gli utenti verranno popolati dinamicamente qui -->
                                </select>
                            </div>
                            <div class="col-md-2 col-xs-12">
                                <label for="group_0_[phase_id]">Fase</label>
                                <select name="group[0][phase_id]" id="group_0_phase_id" class="form-control" required>
                                    <option value="">Seleziona...</option>
                                    <!-- Le fasi verranno popolate dinamicamente qui -->
                                </select>
                            </div>
                            <div class="col-md-2 col-xs-12">
                                <label for="group_0_group">Nome Gruppo</label>
                                <input type="text" name="group[0][group]" id="group_0_group" class="form-control"
                                       placeholder="Gruppo" required>
                            </div>
                            <div class="col-md-2 col-xs-12">
                                <label for="group_0_bonus">Bonus</label>
                                <input type="text" name="group[0][bonus]" id="group_0_bonus" class="form-control"
                                       placeholder="Bonus">
                            </div>
                            <div class="col-md-3 col-xs-12">
                                <label for="btn-add-group">Aggiungi</label>
                                <button id="btn-add-group" type="button"
                                        class="form-control btn btn-primary btn-block btn-plus">+
                                </button>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3 offset-md-9 col-xs-12 pull-right m-t">
                        <button type="submit"
                                class="form-control btn btn-primary btn-block btn-flat">{{ trans('custom.save') }}</button>
                    </div>
                </div>

            </form>
        </div>
    </div>

@stop

@section('js')
    <script src="{{ asset('js/plugin/jquery.datetimepicker.full.min.js') }}"></script>
    <script src="{{ asset('js/plugin/datatables/datatables.min.js') }}"></script>

    <script>
        document.addEventListener('DOMContentLoaded', function () {
            // Funzione per popolare le select di utenti e fasi
            function populateUserAndPhaseSelects(tournamentId) {
                if (tournamentId) {
                    fetch(`/auth/admin/tournaments/getPhasesAndUsers/${tournamentId}`)
                        .then(function (response) {
                            return response.json();
                        })
                        .then(function (data) {
                            // Trova tutte le select degli utenti e delle fasi in base ai gruppi presenti
                            document.querySelectorAll('[id^=group_][id$=_user_id]').forEach(function(userSelect) {
                                // Svuota e popola la select degli utenti per ogni gruppo
                                userSelect.innerHTML = '<option value="">Seleziona...</option>';
                                if (data.users) {
                                    data.users.forEach(function (user) {
                                        var userOption = document.createElement('option');
                                        userOption.value = user.id;
                                        userOption.text = user.name;
                                        userSelect.appendChild(userOption);
                                    });
                                }
                            });

                            document.querySelectorAll('[id^=group_][id$=_phase_id]').forEach(function(phaseSelect) {
                                // Svuota e popola la select delle fasi per ogni gruppo
                                phaseSelect.innerHTML = '<option value="">Seleziona...</option>';
                                if (data.phases) {
                                    data.phases.forEach(function (phase) {
                                        var phaseOption = document.createElement('option');
                                        phaseOption.value = phase.id;
                                        phaseOption.text = phase.phase;
                                        phaseSelect.appendChild(phaseOption);
                                    });
                                }
                            });
                        })
                        .catch(function (error) {
                            console.error('Errore nel recupero dei dati:', error);
                        });
                } else {
                    // Resetta le select se non viene selezionato alcun torneo
                    document.querySelectorAll('[id^=group_][id$=_user_id]').forEach(function(userSelect) {
                        userSelect.innerHTML = '<option value="">Seleziona...</option>';
                    });
                    document.querySelectorAll('[id^=group_][id$=_phase_id]').forEach(function(phaseSelect) {
                        phaseSelect.innerHTML = '<option value="">Seleziona...</option>';
                    });
                }
            }

            // Ascolta i cambiamenti sulla select del torneo (id unico "tournament_id")
            document.getElementById('tournament_id').addEventListener('change', function () {
                var tournamentId = this.value;
                populateUserAndPhaseSelects(tournamentId);
            });

            $(document).on('click', '.btn-plus', function () {
                var key = $('.group-add').find('.row.group').length;
                var $row = $(this).closest('.group-add').find('.form-group').first().clone();

                // Aggiorna gli ID e i nomi degli input e delle select nel gruppo clonato
                $row.find('input, select').each(function () {
                    var name = $(this).attr('name').replace('[0]', '[' + key + ']');
                    var id = $(this).attr('id').replace('_0_', '_' + key + '_');

                    $(this).attr('name', name);
                    $(this).attr('id', id);
                    $(this).closest('.row').find('label').attr('for', id);

                    // Non resettare le select popolando con le opzioni già esistenti
                    if ($(this).is('select')) {
                        // Mantieni le opzioni esistenti nella select
                        var previousOptions = $(this).html();
                        $(this).html(previousOptions); // Mantieni le opzioni precedenti
                    } else {
                        $(this).val(''); // Resetta i valori degli input normali
                    }
                });

                // Aggiungi la nuova riga al DOM
                $('.group-add').append($row);
            });

            jQuery(document).ready(function ($) {
                $('.datetimepicker').datetimepicker({
                    format: 'Y-m-d H:i'
                });
            });
        });
    </script>
@stop
