@extends('page')

@section('content_header')
    <h1>Admin - Lista fasi</h1>
@stop

@section('title', 'The Betting Game - Elenco Fasi')

@section('content')
    @parent

    @include('partials.messages')

    <div class="card">

        <div class="card-header">
            <a href="{{ route('admin.phases.add') }}" class="btn btn-primary float-right">{{ __('custom.create_new') }}</a>
        </div>

        <div class="card-body">

            <table class="table datatable responsive">
                <thead>
                <tr>
                    <th>
                        Torneo
                    </th>
                    <th>
                        Tipo
                    </th>
                    <th>
                        Nome
                    </th>
                    <th>
                        Numero qualificati
                    </th>
                    <th>
                        Numero playoff
                    </th>
                    <th>
                        Data inizio
                    </th>
                    <th>
                        Data fine
                    </th>
                    <th>
                        Note
                    </th>
                    <th class="text-center">
                        Azioni
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($phases as $phase)
                    <tr>
                        <td>{{ $phase->tournament->name }}</td>
                        <td>{{ $phase->type->name }}</td>
                        <td>{{ $phase->name }}</td>
                        <td>{{ $phase->n_qualified }}</td>
                        <td>{{ $phase->n_playoff }}</td>
                        <td>{{ \Carbon\Carbon::parse($phase->start_date)->format('Y-m-d H:i') }}</td>
                        <td>{{ \Carbon\Carbon::parse($phase->end_date)->format('Y-m-d H:i') }}</td>
                        <td>{{ $phase->notes }}</td>
                        <td class="text-center">
                            <a href="{{ route('admin.phases.edit', ['phase' => $phase->id]) }}">
                                <button type="button" class="btn btn-warning btn-xs dt-edit" style="margin-right:16px;">
                                    <i class="fas fa-edit"></i>
                                </button>
                            </a>

                            <!-- Form per la cancellazione -->
                            <form action="{{ route('admin.phases.destroy', ['phase' => $phase->id]) }}" method="POST" style="display:inline;">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger btn-xs delete-phase-btn" data-id="{{ $phase->id }}">
                                    <i class="fas fa-trash"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop
@include('partials.datatable')
@section('js')
@section('plugins.Sweetalert2', true)

    <script>
        $(document).ready(function () {
            $('.delete-phase-btn').click(function (e) {
                e.preventDefault(); // Prevent default form submission

                Swal.fire({
                    title: '{{ __('custom.are_you_sure') }}',
                    text: '{{ __('custom.cannot_undo_this_action') }}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '{{ __('custom.delete') }}',
                    cancelButtonText: '{{ __('custom.no') }}',
                }).then((result) => {
                    console.log(result);
                    if (result && !result.hasOwnProperty('dismiss')) {
                        $(e.target).closest('form').submit();
                    }
                });
            });
        });
    </script>
@endsection
