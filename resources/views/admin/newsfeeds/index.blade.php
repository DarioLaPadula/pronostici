@extends('page')

@section('title', 'Lista news')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/plugin/datatables/datatables.min.css') }}">
@stop

@section('content_header')
    <h1>Admin - Lista News</h1>
@stop

@section('content')
    @parent

    <div class="row">
        <div class="col-md-12">
            @if(Session::has('message'))
                <div class="alert alert-success">
                    {{ Session::get('message') }}
                </div>
            @endif

            <table class="datatable">
                <thead>
                <th>
                    Data
                </th>
                <th>
                    Titolo
                </th>
                <th>
                    Torneo
                </th>
                </thead>
                <tbody>
                @foreach($newsfeeds as $news)
                    <tr>
                        <td>
                            {{\Carbon\Carbon::parse($news->created_at)}}
                        </td>
                        <td>
                            {{$news->tournament()->first() ? $news->tournament()->first()->name : __('custom.no_tournament')}}
                        </td>
                        <td>
                            {{$news->title}}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('js')
    <script src="{{ asset('js/plugin/datatables/datatables.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('.datatable').DataTable();
        });
    </script>
@stop
