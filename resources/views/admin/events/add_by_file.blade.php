@extends('page')

@section('content_header')
    <h1>Admin - Aggiungi eventi</h1>
@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('css/plugin/jquery.datetimepicker.min.css') }}">
@stop

@section('content')
    @parent

    <form action="{{ route('admin.event.store') }}" method="post">
        {!! csrf_field() !!}
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-xs-12 col-md-4">
                        <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                            <select name="category_id" class="form-control">
                                @foreach ($categories as $c)
                                    <option {{ $categoryId === $c->id ? "selected" : ""}} value="{{ $c->id }}">{{ $c->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-4">
                        <div class="form-group has-feedback {{ $errors->has('name') ? 'has-error' : '' }}">
                            <input type="text" name="name" class="form-control" value="{{ $contents[0] }}"
                                   placeholder="{{ trans('custom.name') }}">
                            @if ($errors->has('name'))
                                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-3">
                        <div class="form-group has-feedback {{ $errors->has('deadline') ? 'has-error' : '' }}">
                            <input type="text" name="deadline" class="form-control datetimepicker"
                                   value="{{ $contents[1] }}" placeholder="{{ trans('custom.date') }}">
                            @if ($errors->has('name'))
                                <span class="help-block">
                    <strong>{{ $errors->first('deadline') }}</strong>
                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-1">
                        <div class="form-group has-feedback {{ $errors->has('private') ? 'has-error' : '' }}">
                            <label for="private">{{ trans('custom.private') }}</label>
                            <input name="private" type="checkbox" value="{{ old('private') ? old('private') : 1 }}">
                            @if ($errors->has('private'))
                                <span class="help-block">
                    <strong>{{ $errors->first('private') }}</strong>
                </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="bets">
                    @if (isset($contents) && ($contents))
                        @foreach($contents as $key => $content)
                            @if (($key > 1) && ($key % 2 == 0) && isset($contents[$key+1]))
                                <div class="form-group row m-t" data-number="{{ $key }}">
                                    <div class="col-md-5 col-xs-12">
                                        <input type="text" name="bet.name[{{ $key }}]" class="form-control name"
                                               value="{{ $contents[$key] }}"
                                               placeholder="{{ trans('custom.name') }}">
                                    </div>
                                    <div class="col-md-5 col-xs-12">
                                        <input type="text" name="bet.points[{{ $key }}]" data-number="{{ $key }}"
                                               class="form-control points" value="{{ $contents[$key+1] }}"
                                               placeholder="{{ trans('custom.points') }}">
                                    </div>
                                    <div class="col-md-1 col-xs-12">
                                        <button type="button"
                                                class="form-control btn btn-primary btn-block btn-flat btn-plus">+
                                        </button>
                                    </div>
                                    <div class="col-md-1 col-xs-12">
                                        <button type="button"
                                                class="form-control btn btn-danger btn-block btn-flat btn-minus">-
                                        </button>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    @else
                        <div class="form-group row m-t" data-number="0">
                            <div class="col-md-5 col-xs-12">
                                <input type="text" name="bet.name[0]" class="form-control name"
                                       value=""
                                       placeholder="{{ trans('custom.name') }}">
                            </div>
                            <div class="col-md-5 col-xs-12">
                                <input type="text" name="bet.points[0]" data-number="0"
                                       class="form-control points" value=""
                                       placeholder="{{ trans('custom.points') }}">
                            </div>
                            <div class="col-md-1 col-xs-12">
                                <button type="button"
                                        class="form-control btn btn-primary btn-block btn-flat btn-plus">+
                                </button>
                            </div>
                            <div class="col-md-1 col-xs-12">
                                <button type="button"
                                        class="form-control btn btn-danger btn-block btn-flat btn-minus">-
                                </button>
                            </div>
                        </div>
                    @endif
                </div>

                <div class="row">
                    <div class="col-md-3 offset-md-9 col-xs-12 pull-right m-t">
                        <button type="submit"
                                class="form-control btn btn-primary btn-block btn-flat">{{ trans('custom.save') }}</button>
                    </div>
                </div>
            </div>
        </div>

    </form>

@stop

@section('js')
    <script src="{{ asset('js/plugin/jquery.datetimepicker.full.min.js') }}"></script>
    <script>
        jQuery(document).ready(function ($) {
            $('.datetimepicker').datetimepicker({
                format: 'Y-m-d H:i'
            });
        });
    </script>
    <script>
        // @TODO: MIGLIORARE
        $(document).find('.btn-plus').click(function () {
            var row_original = $(this).closest('.bets').find('.row').last();
            var row = $($(this).closest('.row').clone());
            var number = parseInt($(row_original).data('number'));

            row.find('.name').attr('name', 'bet.name[' + parseInt(number + 1) + ']');
            row.find('.points').attr('name', 'bet.points[' + parseInt(number + 1) + ']');

            row.addClass('m-t');
            row.data('number', number + 1);

            $('.bets').append(row);
        });

        // @TODO: MIGLIORARE
        $(document).find('.btn-minus').click(function () {
            $(this).closest('.form-group').remove();
        });

    </script>
@stop
