<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
    <title>@yield('title_prefix', config('adminlte.title_prefix', ''))
        @yield('title', config('adminlte.title', 'AdminLTE 2'))
        @yield('title_postfix', config('adminlte.title_postfix', ''))</title>
    <!-- CSS  -->
    <link href="{{ asset('css/plugin/material/icons.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugin/material/material.min.css') }}" type="text/css" rel="stylesheet"
          media="screen,projection"/>
    <link href="{{ asset('css/plugin/material/style.css') }}" type="text/css" rel="stylesheet"
          media="screen,projection"/>

</head>
<body>
<nav class="bg-blue" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="#" class="brand-logo" style="margin-top: 10px">
            <img src="{{ asset('img/favicon.png') }}" style="width:50px; height:50px" rel="shortcut icon">
        </a>
        <ul class="right hide-on-med-and-down">
            <li><a class="modal-trigger"
                   href="#modal-login">{{ __('adminlte.sign_in') }}</a></li>
        </ul>
        <ul class="right hide-on-med-and-down">
            <li><a class="modal-trigger"
                   href="#modal-signup">{{ __('adminlte.register_message') }}</li>
        </ul>

        <ul class="right hide-on-med-and-down">
            <li><a class="modal-trigger"
                   href="{{ url(config('adminlte.password_reset_url', 'password/reset')) }}">{{ __('adminlte.i_forgot_my_password') }}
            </li>
        </ul>

        <ul class="right hide-on-med-and-down">
            <li><a class="modal-trigger"
                   href="#modal-regulation">{{ __('custom.regulation') }}</li>
        </ul>

        <ul id="nav-mobile" class="sidenav bg-blue">
            <li><a class="modal-trigger"
                   href="#modal-login"><span class="white-text">{{ __('adminlte.sign_in') }}</span></a></li>
            <li><a class="modal-trigger"
                   href="#modal-signup"><span class="white-text">{{ __('adminlte.register_message') }}</span></a></li>
            <li><a class="modal-trigger"
                   href="{{ url(config('adminlte.password_reset_url', 'password/reset')) }}"><span
                            class="white-text">{{ __('adminlte.i_forgot_my_password') }}</span></a>
            </li>
            <li><a class="modal-trigger"
                   href="#modal-regulation"><span class="white-text">{{ __('custom.regulation') }}</span></a></li>
        </ul>

        <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
    </div>
</nav>
<div class="parallax-container">
    <div class="parallax"><img src="{{ asset('img/uefa-champions-league-stadium.jpg') }}"></div>
    <div class="section no-pad-bot" id="index-banner">
        <div class="container">
            <h1 class="header center red-text">The Betting Game</h1>
            <div class="row center">
                <h5 class="header col s12 light white-text">Il gioco a pronostici <strong>GRATUITO</strong> con le quote
                    dei
                    bookmaker
                </h5>
            </div>
            <div class="row center">
                <div class="col l6 m8 s12 offset-m2 offset-l3 center-align">
                    <a class="waves-effect waves-light btn btn-circle btn-header-title modal-trigger"
                       href="#modal-login">{{ __('adminlte.sign_in') }}</a>
                </div>
            </div>

            <div class="row center">
                <div class="col l6 m8 s12 offset-m2 offset-l3 center-align">
                    <a class="waves-effect waves-light btn btn-circle btn-header-title modal-trigger"
                       href="#modal-signup">{{ __('adminlte.register_message') }}</a>
                </div>
            </div>

            <div class="row center">
                <div class="col l6 m8 s12 offset-m2 offset-l3 center-align">
                    <a class="waves-effect waves-light btn btn-circle btn-header-title modal-trigger"
                       href="{{ url(config('adminlte.password_reset_url', 'password/reset')) }}">{{ __('adminlte.i_forgot_my_password') }}</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="section">

        <!--   Icon Section   -->
        <div class="row">
            <div class="col s12 m6">
                <div class="icon-block">
                    <h2 class="center light-blue-text"><i class="material-icons">flash_on</i></h2>
                    <h5 class="center">Gioco con le quote</h5>

                    <p class="light">Gioca contro altri utenti, in modo totalmente gratuito, puntando la tua scommessa.
                        Più difficile l'esito pronosticato più punti guadagni<br>
                        <br>
                        Si punta su uno degli esiti disponibili per ogni evento e, in caso di risultato
                        indovinato, si riceve un punteggio pari al valore della quota puntata.<br></p>
                </div>
            </div>

            <div class="col s12 m6">
                <div class="icon-block">
                    <h2 class="center light-blue-text"><i class="material-icons">emoji_events</i></h2>
                    <h5 class="center">Semplice modalità di punteggio</h5>

                    <p class="light">
                        Il punteggio totale del partecipante sarà la somma delle quote indovinate.<br>
                        <br>
                        Un evento non indovinato o non pronosticato varrà 0 punti.<br>
                        <br>
                        Si può modificare ogni pronostico quante si vuole entro l'orario per cui è fissato il calcio
                        d'inizio del match.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!--<div class="parallax-container">
    <div class="parallax"><img src="{{ asset('img/uefa-champions-league-stadium.jpg') }}"></div>
</div>-->
<div class="container">
    <div class="section">
        <div class="row">
            <div class="col s12 m12">
                <div class="icon-block">
                    <h2 class="center light-blue-text"><i class="material-icons">sports_soccer</i></h2>
                    <h5 class="center">Preparati per la Champions League 2021/22!</h5>

                    <p class="light">Due modalità di torneo, una che tiene conto solo del punteggio totale (a cui
                        potranno partecipare tutti dal primo match fino alla finale) e l'altra con gironi + eliminazione
                        diretta!<br>
                        <br>
                        Nella prima fase i partecipanti saranno suddivisi in 8 gironi, di cui solo i due migliori
                        punteggi del raggruppamento, al termine della prima fase, accederanno agli ottavi. In caso di
                        parità, conteranno, nell'ordine:<br>
                        -chi ha indovinato il maggior numero di eventi;<br>
                        -chi ha indovinato la quota più alta;<br>
                        -chi ha dimenticato meno eventi;<br>
                        -chi si è unito prima al torneo.<br>
                        <br>
                        Agli ottavi verrà stilato il tabellone tennistico dei 16 partecipanti rimasti. Nel sorteggiare
                        gli ottavi, le prime classificate sfideranno le seconde e avranno un bonus di partenza di 2
                        punti.<br>
                        <br>
                        Altre eventuali modalità della fase a eliminazione diretta verranno stabilite in seguito.<br>
                        <br>
                        Gli eliminati della fase a gironi potranno ripartire, da Febbraio, nel torneo dell'Europa League
                        insieme a nuovi eventuali iscritti.<br>
                        <br>Nella fase a gironi della Champions League, i voti saranno tutti pubblici, tranne
                        nell'ultima giornata. Dalla fase a
                        eliminazione diretta sarà possibile visualizzare i voti dell'avversario solo dopo la chiusura
                        dei pronostici (inizio della partita).<br>
                        <br>
                        Nella fase a eliminazione diretta Il pronostico data è da considerarsi nei 90 minuti della
                        partita (quindi senza contare eventuali supplementari e rigori).

                    </p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <h4 class="text-success valign center">
                    <strong>Che aspetti ad unirti? Le iscrizioni chiudono il 14 Settembre alle ore 13.00</strong>
                </h4>
            </div>
            <div class="row center">
                <a href="#modal-signup" id="download-button"
                   class="btn-large btn-success waves-effect waves-light modal-trigger">Unisciti Gratis!</a>
            </div>
        </div>

    </div>
    <br><br>
</div>

@include('landing.partials.modal_login')
@include('landing.partials.modal_signup')
@include('landing.partials.modal_regulation')

<footer class="page-footer grey">
    <div class="container" style="padding-bottom: 40px;">
        <div class="row">
            <div class="col s12">
                Made by <a class="text-lighten-3" href="https://www.linkedin.com/in/dario-la-padula-ab770780/">Dario La
                    Padula</a>
            </div>
        </div>
        <div class="row" style="margin-bottom: 0;">
            <div class="col s6 right-align">
                <a href="https://t.me/tuttocalcionews">
                    <img class="circle responsive-img margin" style="width: 100px; height: 100px"
                         src="{{ asset('img/tcn.png') }}">
                </a>
            </div>
            <div class="col s6">
                <a href="https://t.me/BarSportNews">
                    <img class="circle responsive-img" style="width: 100px; height: 100px"
                         src="{{ asset('img/barsportnewslogo.jpg') }}">
                </a>
            </div>

        </div>
    </div>
</footer>

<!--  Scripts-->
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="{{ asset('js/plugin/material/materialize.js') }}"></script>
<script src="{{ asset('js/plugin/material/init.js') }}"></script>

<script>
    var options = {};
    /* document.addEventListener('DOMContentLoaded', function() {
         var elems = document.querySelectorAll('.modal');
         var instances = M.Modal.init(elems, options);
     });
     */
</script>
<script>
    $(".btn-refresh").click(function () {
        $.ajax({
            type: 'GET',
            url: '{{ route('refresh_captcha') }}',
            success: function (data) {
                $(".captcha span").html(data.captcha);
            }
        });
    });
</script>


<script>
    $(document).ready(function () {
        $('.modal').modal();
        @if(session()->get('error') == 'login')
        $(function () {
            $('#modal-login').modal('open');
        });
        @endif
        @if(session()->get('error') == 'signup')
        $(function () {
            $('#modal-signup').modal('open');
        });
        @endif
    });

</script>
</body>
</html>
