<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords" content="Bootstrap, Landing page, Template, Registration, Landing">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="author" content="Grayrids">
    <title>The Betting Game</title>
    <link rel="icon" type="image/x-icon" href="{{ asset('img/logo.png') }}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/essence/bootstrap.min.css">
    <link rel="stylesheet" href="css/essence/main.css">

    <!-- Styles -->
    <style>
        .rules li {
            list-style-type: circle;
            position: relative;
            left: 30px;
        }

        p {
            margin-bottom: 20px;
        }

        div {
            margin-bottom: 10px;
        }
    </style>

</head>

<body>
<div class="container">
    <div class="row">

        <div class="col-12 text-center">
            <h3>Regole The Betting Game</h3>
        </div>
        <div class="col-12 mt-10">
            <p class="modal-subtitle">Regolamento Generale</p>
        </div>
    </div>

    @include('partials.general_regulation')

    @if($lastTournament)
        <div class="row mt-10">
            <div class="col-12">
                <p class="modal-subtitle mt-10">{{ $lastTournament->title }}</p>
            </div>
            <div class="col-12 mt-10">
                {!! $lastTournament->news !!}
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-12">
            <p>Per iscriversi, andare su "Tornei Disponibili" entro il 13 Febbraio alle ore 18.45 e cliccare su
                "Iscriviti".</p>
        </div>
    </div>
</div>

<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="js/essence/jquery-min.js"></script>
<script src="js/essence/bootstrap.min.js"></script>

</body>
</html>
