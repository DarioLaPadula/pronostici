<!-- Contact Section Start -->
<section id="contact" class="section">
    <div class="contact-form">
        <div class="container">
            <div class="section-header">
                <h2 class="section-title">{{ __('landing.contact.section_title') }}</h2>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="contact-block">
                        <form id="contactForm" method="POST" action="{{ route('landing.send_contact_email') }}">
                            <input type="hidden" name="full_name" value="">
                            <input type="hidden" name="form_start_time" value="{{ now()->timestamp }}">
                            @csrf
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="name" name="name" placeholder="{{ __('landing.contact.name_placeholder') }}" required data-error="{{ __('landing.contact.name_error') }}">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="email" placeholder="{{ __('landing.contact.email_placeholder') }}" id="email" class="form-control" name="email" required data-error="{{ __('landing.contact.email_error') }}">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea class="form-control" id="message" name="message" placeholder="{{ __('landing.contact.message_placeholder') }}" rows="7" data-error="{{ __('landing.contact.message_error') }}" required></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="submit-button text-right">
                                        <button class="btn btn-common btn-effect" id="submitBtn" type="button">
                                            {{ __('landing.contact.submit_button') }}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Contact Section End -->
