<!-- Modal Body -->
<div id="modal-regulation" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title">
                    {{ trans('custom.regulation') }}
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">

                <div class="row">

                    <div class="col-12 text-center">
                        <h3>Regole The Betting Game</h3>
                    </div>
                    <div class="col-12 mt-10">
                        <p class="modal-subtitle">Regolamento Generale</p>
                    </div>

                    @include('partials.general_regulation')
                </div>
                @if($lastTournament)
                    <div class="row mt-10">
                        <div class="col-12">
                            <p class="modal-subtitle mt-10">{{ $lastTournament->title }}</p>
                        </div>
                        <div class="col-12 mt-10">
                            {!! $lastTournament->news !!}
                        </div>
                    </div>
                @endif
            </div>

            <div class="modal-footer">

                <button type="button" class="btn btn-secondary btn-circle btn-large"
                        data-dismiss="modal" aria-label="Close">
                    {{ __('custom.close') }}
                </button>
            </div>
        </div>
    </div>
</div>
