@section('js')
    @parent
    <script>
        $(document).ready(function () {
            $(document).on('click', 'a.event', function(e) {
                console.log('click');
                $("#loadingoverlay").fadeIn();
                var $show_results = $(this).closest('.match').find('#show_results')
                console.log($(this));
                if (!$show_results.length) {
                    e.preventDefault();
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType: 'html',
                        method: "POST",
                        url: '{{ route('pages.eventAjax') }}',
                        data: {
                            'event_id': $(this).data('event_id'),
                            'tournament_id': $(this).data('tournament_id'),
                            '_token': "{{ csrf_token() }}",
                        },
                        success: function (data) {
                            var modal = $('#extra-large-modal');
                            modal.find('.modal-title').html($(data).data('event'));
                            modal.find('.modal-body').html($(data));
                            modal.modal('show');
                        },
                        complete: function () {
                            $("#loadingoverlay").fadeOut();
                        },
                    });
                } else {
                    $show_results.hide("slow", function () {
                        $(this).remove();
                    });
                }
            });
        });
    </script>
@endsection
