@section('css')
    @parent
    <link rel="stylesheet" href="{{ asset('css/plugin/datatables/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/plugin/datatables/datatables-responsive.min.css') }}">
@stop

@section('js')
    @parent
    <script src="{{ asset('js/plugin/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('js/plugin/datatables/datatables-responsive.min.js') }}"></script>
    <script>
        var pageLength = 10;

        function doDatatable() {
            // Seleziona tutte le tabelle con la classe 'datatable' che non hanno la classe 'dataTable'
            $('.datatable').not('.dataTable').each(function() {
                $(this).DataTable({
                    "pageLength": pageLength,
                    "searching": false,
                    "order": [],
                    "lengthChange": false,
                    "info": false,
                    "fnDrawCallback": function (oSettings) {
                        var currentPage = oSettings._iDisplayStart / oSettings._iDisplayLength + 1;

                        // Se è una sola pagina, nascondo la paginazione
                        if (($('.datatable tr').length < pageLength) && (currentPage == 1)) {
                            $('.dataTables_paginate').hide();
                        }
                    },
                    "bSort": false,
                    "autoWidth": false,
                    "responsive": true,
                    "language": {
                        "sSearch": "{{ __('datatables.sSearch') }}",
                        "sZeroRecords": "{{ __('datatables.sZeroRecords') }}",
                        "oPaginate": {
                            "sFirst": "{{ __('datatables.sFirst') }}",
                            "sPrevious": "{{ __('datatables.sPrevious') }}",
                            "sNext": "{{ __('datatables.sNext') }}",
                            "sLast": "{{ __('datatables.sLast') }}",
                        },
                    },

                    @if (isset($paging))
                    "paging": {{ $paging }},
                    @endif
                });
            });
        }

        $(document).ready(function () {

            doDatatable();

            // Recalculate responsive behavior on tab switch
            $('a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
                var target = $(e.target).attr('href');
                $(target + ' .datatable').each(function (index, element) {
                    if ($.fn.DataTable.isDataTable(element)) {
                        $(element).DataTable().columns.adjust().responsive.recalc();
                    }
                });
            });

            // Recalculate responsive behavior on window resize
            $(window).on('resize', function () {
                $('.datatable').DataTable().columns.adjust().responsive.recalc();
            } );

        });
    </script>
@endsection
