<!-- Navbar Right Menu -->
<div class="navbar-custom-menu">

    <ul class="nav navbar-nav">
        <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <img class="user-image" src="{{ auth()->user()->getProfileImage() }}">
                <span class="hidden-xs">{{ auth()->user()->name }}</span>
            </a>
            <ul class="dropdown-menu">
                <!-- User image -->
                <li class="user-header">
                    <img class="img-circle" src="{{ auth()->user()->getProfileImage() }}">
                    <p>
                        {{ auth()->user()->name }}
                        <small>{{ __('custom.member_since') . ' ' . \Carbon\Carbon::parse(auth()->user()->created_at)->format('d-m-Y') }}</small>
                    </p>
                </li>
                {{--<!-- Menu Body -->
                <li class="user-body">
                    <div class="row">
                        <div class="col-xs-4 text-center">
                            <a href="#">Followers</a>
                        </div>
                        <div class="col-xs-4 text-center">
                            <a href="#">Sales</a>
                        </div>
                        <div class="col-xs-4 text-center">
                            <a href="#">Friends</a>
                        </div>
                    </div>
                    <!-- /.row -->
                </li>--}}
                <!-- Menu Footer-->
                <li class="user-footer">
                    <div class="float-left">
                        <a href="{{ route('auth.user.edit') }}" class="btn btn-default btn-flat"><i class="fa fa-fw fa-user"></i>{{ __('custom.profile') }}</a>
                    </div>
                    <div class="float-right">
                        <a href="#" class="btn btn-default btn-flat"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <i class="fa fa-fw fa-power-off"></i> {{ trans('adminlte::adminlte.log_out') }}
                        </a>
                        <form id="logout-form" action="{{ url(config('adminlte.logout_url', 'auth/logout')) }}" method="POST" style="display: none;">
                            @if(config('adminlte.logout_method'))
                                {{ method_field(config('adminlte.logout_method')) }}
                            @endif
                            {{ csrf_field() }}
                        </form>
                    </div>
                </li>
            </ul>
        </li>
        <li>
            @if(config('adminlte.logout_method') == 'GET' || !config('adminlte.logout_method') && version_compare(\Illuminate\Foundation\Application::VERSION, '5.3.0', '<'))

            @else

            @endif
        </li>
    @if(config('adminlte.right_sidebar') and (config('adminlte.layout') != 'top-nav'))
        <!-- Control Sidebar Toggle Button -->
            <li>
                <a href="#" data-toggle="control-sidebar" @if(!config('adminlte.right_sidebar_slide')) data-controlsidebar-slide="false" @endif>
                    <i class="{{config('adminlte.right_sidebar_icon')}}"></i>
                </a>
            </li>
        @endif
    </ul>
</div>